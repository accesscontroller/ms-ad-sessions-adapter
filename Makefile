prepare-dirs:
	mkdir -p release

build: prepare-dirs
	GOOS=windows go build -o release/adapter.exe

release: build

build-race: prepare-dirs
	CGO_ENABLED=1 \
	GOOS=windows \
	GOARCH=amd64 \
	CXX_FOR_TARGET=x86_64-w64-mingw32-g++ \
	CC_FOR_TARGET=x86_64-w64-mingw32-gcc \
	go build -o release/adapter.exe -race