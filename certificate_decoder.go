package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"fmt"
)

// decodeCA decodes base64 encoded certificates and private keys and returns
// built CertPool with CA certificate.
func decodeCA(ca string) (*x509.CertPool, error) {
	var caCerts = x509.NewCertPool()

	caCertRaw, err := base64.StdEncoding.DecodeString(ca)
	if err != nil {
		return nil, fmt.Errorf("can not base64 decode CA Cert: %w", err)
	}

	// Load Certificates.
	if !caCerts.AppendCertsFromPEM(caCertRaw) {
		return nil, fmt.Errorf("can not load CA Cert")
	}

	return caCerts, nil
}

// decodeClientCerts decodes base64 encoded certificates and private keys and returns
// built CertPool with client certificates.
func decodeClientCerts(clientCert, clientKey string) (tls.Certificate, error) {
	clientCertRaw, err := base64.StdEncoding.DecodeString(clientCert)
	if err != nil {
		return tls.Certificate{}, fmt.Errorf("can not base64 decode Client Cert: %w", err)
	}

	clientKeyRaw, err := base64.StdEncoding.DecodeString(clientKey)
	if err != nil {
		return tls.Certificate{}, fmt.Errorf("can not base64 decode Client Key: %w", err)
	}

	cert, err := tls.X509KeyPair(clientCertRaw, clientKeyRaw)
	if err != nil {
		return tls.Certificate{}, fmt.Errorf("can not load client certificate: %w", err)
	}

	return cert, nil
}
