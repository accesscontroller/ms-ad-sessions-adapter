package main

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/ldapclient"
	"gopkg.in/yaml.v2"
)

var (
	ErrParseConfig = errors.New("configuration error")
)

type Logging struct {
	// LogLevel contains minimal logging level to write.
	// Default is ERROR.
	LogLevel logrus.Level

	// LogOutput output destination for log.
	// Default destination is stderr.
	LogOutput string
}

func (l *Logging) UnmarshalYAML(unmarshal func(interface{}) error) error {
	// Unmarshal raw data.
	var tmp struct {
		// LogLevel contains minimal logging level to write.
		// Default is ERROR.
		LogLevel string `json:"logLevel,omitempty" yaml:"logLevel,omitempty"`

		// LogOutput output destination for log.
		// Default destination is stderr.
		LogOutput string `json:"logOutput,omitempty" yaml:"logOutput,omitempty"`
	}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	// Parse LogLevel.
	lvl, err := parseLogLevel(tmp.LogLevel)
	if err != nil {
		return err
	}

	// Parse LogOutput.
	out := parseLogOutput(tmp.LogOutput)

	l.LogLevel = lvl
	l.LogOutput = out

	return nil
}

// LDAPClientConfig contains configuration to connect to LDAP Server.
type LDAPClientConfig struct {
	// Server defines LDAP server hostname and port.
	Server string

	// Username defines LDAP server authentication user name.
	Username string

	// Password defines LDAP server authentication password.
	Password string

	// BaseDN defines LDAP base lookup DN.
	BaseDN string

	// TLSMode defines TLS mode and supports values: TLS, NoTLS, StartTLS.
	TLSMode ldapclient.TLSMode

	// TLSConfig defines Client and CA certificates to connect to TLS LDAP Server.
	TLSConfig *tls.Config
}

func (l *LDAPClientConfig) UnmarshalYAML(unmarshal func(interface{}) error) error {
	// Raw Data.
	var tmp struct {
		// Server defines hostname and port of LDAP Server to connect to.
		Server string `json:"server" yaml:"server"`

		// Username defines authentication credentials username.
		Username string `json:"username" yaml:"username"`

		// Password defines authentication credentials password.
		Password string `json:"password" yaml:"password"`

		// BaseDN defines LDAP DN to start full tree search from.
		BaseDN string `json:"baseDN" yaml:"baseDN"`

		// Defines TLS mode and supports values: TLS, NoTLS, StartTLS.
		TLSMode string `json:"tlsMode" yaml:"tlsMode"`

		// TLSConfig defines Server and Client certificates used during LDAP connection.
		TLSConfig struct {
			// ServerCACertificate defines base64 encoded Server CA Certificate to verify
			// TLS protected LDAP Server.
			ServerCACertificate string `json:"serverCACertificate" yaml:"serverCACertificate"`

			// ClientCertificate defines Client Certificate to use in TLS Client authentication.
			ClientCertificate string `json:"clientCertificate,omitempty" yaml:"clientCertificate,omitempty"`

			// ClientPrivateKey defines Client Private Key to use in TLS Client authentication.
			ClientPrivateKey string `json:"clientPrivateKey,omitempty" yaml:"clientPrivateKey,omitempty"`
		} `json:"tlsConfig,omitempty" yaml:"tlsConfig,omitempty"`
	}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	// Validate.
	tlsM, err := ldapclient.ParseTLSMode(tmp.TLSMode)
	if err != nil {
		return fmt.Errorf("unsupported TLSMode %q given: %w", tmp.TLSMode, err)
	}

	if tmp.Server == "" {
		return fmt.Errorf("%w: empty Server given", ErrParseConfig)
	}

	if tmp.BaseDN == "" {
		return fmt.Errorf("%w: empty BaseDN given", ErrParseConfig)
	}

	// Load and check certificates.
	if tlsM != ldapclient.NoTLS {
		// CA.
		if tmp.TLSConfig.ServerCACertificate == "" {
			return fmt.Errorf("%w: LDAP: empty TLSConfig.ServerCACertificate given", ErrParseConfig)
		}

		caCerts, err := decodeCA(tmp.TLSConfig.ServerCACertificate)
		if err != nil {
			return err
		}

		l.TLSConfig = &tls.Config{
			RootCAs: caCerts,
		}

		// Client.
		if tmp.TLSConfig.ClientCertificate == "" && tmp.TLSConfig.ClientPrivateKey != "" ||
			tmp.TLSConfig.ClientCertificate != "" && tmp.TLSConfig.ClientPrivateKey == "" {
			return fmt.Errorf(
				"%w: LDAP: one of TLSConfig.ClientCertificate or TLSConfig.ClientPrivateKey is empty", ErrParseConfig)
		}

		// If both client certificates are given - decode.
		if tmp.TLSConfig.ClientCertificate != "" && tmp.TLSConfig.ClientPrivateKey != "" {
			clientCerts, err := decodeClientCerts(tmp.TLSConfig.ClientCertificate, tmp.TLSConfig.ClientPrivateKey)
			if err != nil {
				return err
			}

			l.TLSConfig.Certificates = append(l.TLSConfig.Certificates, clientCerts)
		}
	}

	// Set values.
	l.BaseDN = tmp.BaseDN
	l.Password = tmp.Password
	l.Server = tmp.Server
	l.TLSMode = tlsM
	l.Username = tmp.Username

	return nil
}

type UsersGroupsSynchronization struct {
	// Enabled if true - perform Users and Groups synchronization.
	Enabled bool

	// IntervalSec interval between periodic synchronizations.
	// Default is 3600 seconds.
	Interval time.Duration

	// UsersGroupsSource contains a name of ExternalUserV1s and ExternalGroupV1s
	// ExternalUsersGroupsSourceV1 for synchronization.
	UsersGroupsSource storage.ResourceNameT

	// GroupNames defines list of group names to
	// synchronize user-members from.
	GroupNames []storage.ResourceNameT

	// LDAPSettings contains configuration to connect to LDAP server to get Users and Groups.
	LDAPSettings LDAPClientConfig
}

func (l *UsersGroupsSynchronization) UnmarshalYAML(unmarshal func(interface{}) error) error {
	// Raw data.
	var tmp struct {
		// Enabled if true - perform Users and Groups synchronization.
		Enabled bool `json:"enabled,omitempty" yaml:"enabled,omitempty"`

		// IntervalSec interval between periodic synchronizations.
		IntervalSec int `json:"intervalSec,omitempty" yaml:"intervalSec,omitempty"`

		// UsersGroupsSource contains a name of ExternalUserV1s and ExternalGroupV1s
		// ExternalUsersGroupsSourceV1 for synchronization.
		UsersGroupsSource storage.ResourceNameT `json:"usersGroupsSource,omitempty" yaml:"usersGroupsSource,omitempty"`

		// GroupNames defines list of group names to
		// synchronize user-members from.
		GroupNames []storage.ResourceNameT `json:"groupNames,omitempty" yaml:"groupNames,omitempty"`

		// LDAPSettings contains configuration to connect to LDAP server to get Users and Groups.
		LDAPSettings LDAPClientConfig `json:"ldapSettings,omitempty" yaml:"ldapSettings,omitempty"`
	}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	// Validate.
	if tmp.Enabled && (len(tmp.UsersGroupsSource) == 0 || len(tmp.GroupNames) == 0) {
		return fmt.Errorf("%w: invalid parameters: UsersGroupsSynchronization is enabled "+
			"but either UsersGroupsSource or GroupNames are empty", ErrParseConfig)
	}

	l.Enabled = tmp.Enabled
	l.Interval = time.Duration(tmp.IntervalSec) * time.Second
	l.UsersGroupsSource = tmp.UsersGroupsSource
	l.GroupNames = tmp.GroupNames
	l.LDAPSettings = tmp.LDAPSettings

	return nil
}

type SessionsSynchronization struct {
	// SessionTimeout defines a timeout after which a Session is considered "stale" and closed.
	// Recommended value is more than 7200
	SessionTimeout time.Duration

	// CloseSessionsInterval defines an interval between polls for "stale" sessions.
	CloseSessionsInterval time.Duration

	// WindowsEventLogs contains names of Windows Event Log journals to
	// read Session events from.
	// It is possible to configure Windows Event log to store
	// authentication Logon/Logoff events to a separate log
	// and configure the adapter to read from the log
	// to reduce the adapter cpu and memory usage by feeding it with already filtered
	// records.
	WindowsEventLogs []string

	// SessionsSource contains a name of ExternalSessionsSourceV1
	// for synchronization.
	SessionsSource storage.ResourceNameT
}

func (l *SessionsSynchronization) UnmarshalYAML(unmarshal func(interface{}) error) error {
	// Raw data.
	var tmp struct {
		// SessionTimeout defines a timeout after which a Session is considered "stale" and closed.
		// Recommended value is more than 7200
		SessionTimeoutSec int `json:"sessionTimeoutSec,omitempty" yaml:"sessionTimeoutSec,omitempty"`

		// CloseSessionsInterval defines an interval between polls for "stale" sessions.
		CloseSessionsIntervalSec int `json:"closeSessionsIntervalSec,omitempty" yaml:"closeSessionsIntervalSec,omitempty"`

		// WindowsEventLogs contains names of Windows Event Log journals to
		// read Session events from.
		// It is possible to configure Windows Event log to store
		// authentication Logon/Logoff events to a separate log
		// and configure the adapter to read from the log
		// to reduce the adapter cpu and memory usage by feeding it with already filtered
		// records.
		WindowsEventLogs []string `json:"windowsEventLogs,omitempty" yaml:"windowsEventLogs,omitempty"`

		// SessionsSource contains a name of ExternalSessionsSourceV1
		// for synchronization.
		SessionsSource storage.ResourceNameT `json:"sessionsSource,omitempty" yaml:"sessionsSource,omitempty"`
	}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	// Validate.
	if len(tmp.SessionsSource) == 0 {
		return fmt.Errorf("%w: given SessionsSource is empty", ErrParseConfig)
	}

	// Parse WindowsEventLogs.
	if len(tmp.WindowsEventLogs) == 0 {
		tmp.WindowsEventLogs = []string{"Security"}
	}

	l.SessionsSource = tmp.SessionsSource
	l.WindowsEventLogs = tmp.WindowsEventLogs

	if tmp.SessionTimeoutSec < 1 { // 1 is the minimum adequate session lifetime.
		return fmt.Errorf("%w: sessionTimeoutSec must be 1 or greater, given: %d",
			ErrParseConfig, tmp.CloseSessionsIntervalSec)
	}

	l.SessionTimeout = time.Duration(tmp.SessionTimeoutSec) * time.Second

	if tmp.CloseSessionsIntervalSec < 1 { // 1 is the minimum adequate value.
		return fmt.Errorf("%w: closeSessionsIntervalSec must be 1 or greater, given: %d",
			ErrParseConfig, tmp.CloseSessionsIntervalSec)
	}

	l.CloseSessionsInterval = time.Duration(tmp.CloseSessionsIntervalSec) * time.Second

	return nil
}

type APIServer struct {
	// InsecureConnection if true - allows connection to API server without
	// CA certificate verification.
	InsecureConnection bool

	// URL defines URL to connect to API Server.
	URL *url.URL

	// CACert defined base64 encoded TLS CA cert to use for verifying API Server cert.
	CACert *x509.CertPool

	// ClientCerts defines base64 encoded client public and private authentication key.
	ClientCerts tls.Certificate

	// RequestTimeout defines request timeout.
	// Default is 10 seconds.
	RequestTimeout time.Duration
}

func (l *APIServer) UnmarshalYAML(unmarshal func(interface{}) error) error {
	// Raw data.
	var tmp struct {
		// InsecureConnection if true - allows connection to API server without
		// CA certificate verification.
		InsecureConnection bool `json:"insecureConnection,omitempty" yaml:"insecureConnection,omitempty"`

		// URL defines URL to connect to API Server.
		URL string `json:"url" yaml:"url"`

		// CACert defined base64 encoded TLS CA cert to use for verifying API Server cert.
		CACert string `json:"caCert" yaml:"caCert"`

		// ClientCert defines base64 encoded client public authentication key.
		ClientCert string `json:"clientCert" yaml:"clientCert"`

		// ClientKey defines base64 encoded client private authentication key.
		ClientKey string `json:"clientKey" yaml:"clientKey"`

		// RequestTimeoutSec defines request timeout.
		// Default is 10 seconds.
		RequestTimeoutSec int `json:"requestTimeoutSec,omitempty" yaml:"requestTimeoutSec,omitempty"`
	}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	// Validate.
	if tmp.URL == "" {
		return fmt.Errorf("%w: empty URL given", ErrParseConfig)
	}

	pURL, err := url.Parse(tmp.URL)
	if err != nil {
		return fmt.Errorf("incorrect API Server URL: %w", err)
	}

	// CA Certificates.
	var caCerts *x509.CertPool

	if !tmp.InsecureConnection {
		if tmp.CACert == "" {
			return fmt.Errorf(
				"%w: empty CA certificate while TLS CA verification required", ErrParseConfig)
		}

		// Decode CA Cert.
		ca, e := decodeCA(tmp.CACert)
		if e != nil {
			return fmt.Errorf("can not decode certificates: %w", e)
		}

		caCerts = ca
	}

	// Client Certificates.
	var clientCerts tls.Certificate

	if !tmp.InsecureConnection {
		if tmp.ClientCert == "" || tmp.ClientKey == "" {
			return fmt.Errorf("%w: either empty ClientCert or ClientKey given", ErrParseConfig)
		}

		clCrt, e := decodeClientCerts(tmp.ClientCert, tmp.ClientKey)
		if e != nil {
			return fmt.Errorf("can not decode certificates: %w", err)
		}

		clientCerts = clCrt
	}

	if tmp.RequestTimeoutSec == 0 {
		tmp.RequestTimeoutSec = 10
	}

	// Set values.
	l.CACert = caCerts
	l.ClientCerts = clientCerts
	l.InsecureConnection = tmp.InsecureConnection
	l.URL = pURL
	l.RequestTimeout = time.Duration(tmp.RequestTimeoutSec) * time.Second

	return nil
}

type Config struct {
	// InstanceName defines name of the adapter for logging and monitoring purpose.
	// Default is the system hostname.
	InstanceName string

	// APIServer defines API Server connection configuration.
	APIServer APIServer

	// Sessions defines Windows Sessions synchronization configuration.
	Sessions SessionsSynchronization

	// UsersGroups defines Windows Users and Groups synchronization configuration.
	UsersGroups UsersGroupsSynchronization

	// Log defines logging configuration.
	Log Logging
}

func (l *Config) UnmarshalYAML(unmarshal func(interface{}) error) error {
	// Raw data.
	var tmp struct {
		// InstanceName defines name of the adapter for logging and monitoring purpose.
		// Default is the system hostname.
		InstanceName string `json:"instanceName,omitempty" yaml:"instanceName,omitempty"`

		// APIServer defines API Server connection configuration.
		APIServer APIServer `json:"apiServer" yaml:"apiServer"`

		// Sessions defines Windows Sessions synchronization configuration.
		Sessions SessionsSynchronization `json:"sessions" yaml:"sessions"`

		// UsersGroups defines Windows Users and Groups synchronization configuration.
		UsersGroups UsersGroupsSynchronization `json:"usersGroups" yaml:"usersGroups"`

		// Log defines logging configuration.
		Log Logging `json:"log" yaml:"log"`
	}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	// Set default InstanceName.
	if tmp.InstanceName == "" {
		hn, err := os.Hostname()
		if err != nil {
			return fmt.Errorf("can not get System Hostname to use it as a default InstanceName: %w", err)
		}

		tmp.InstanceName = hn
	}

	// Set result.
	l.APIServer = tmp.APIServer
	l.InstanceName = tmp.InstanceName
	l.Log = tmp.Log
	l.Sessions = tmp.Sessions
	l.UsersGroups = tmp.UsersGroups

	return nil
}

// parseYaml loads Synchronizer configuration from YAML.
func parseYaml(src []byte) (*Config, error) {
	var config Config

	if err := yaml.UnmarshalStrict(src, &config); err != nil {
		return nil, fmt.Errorf("can not decode YAML config: %w", err)
	}

	return &config, nil
}

// parseLogOutput parses and opens (if applicable) log output
// for logging.
// Returns stderr on empty string and stdout on "stdout" string.
func parseLogOutput(n string) string {
	if n == "" {
		return os.Stderr.Name()
	}

	return n
}

// parseLogLevel parses minimal log level.
// Returns ERROR level on empty string given.
func parseLogLevel(n string) (logrus.Level, error) {
	if n == "" {
		return logrus.ErrorLevel, nil
	}

	switch strings.ToUpper(n) {
	case "TRACE":
		return logrus.TraceLevel, nil
	case "DEBUG":
		return logrus.DebugLevel, nil
	case "INFO":
		return logrus.InfoLevel, nil
	case "WARNING":
		return logrus.WarnLevel, nil
	case "ERROR":
		return logrus.ErrorLevel, nil
	case "FATAL":
		return logrus.FatalLevel, nil

	default:
		return 0, fmt.Errorf("%w: unsupported Log Level value %q", ErrParseConfig, n)
	}
}
