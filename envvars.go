package main

import "os"

const (
	EnvVarsPrefix string = "MS_AD_SESSIONS_ADAPTER_"

	DefaultYAMLConfigFilePath string = "adapter_config.yaml"
)

type EnvVars struct {
	ConfigFile string
}

func parseEnv() EnvVars {
	cfp := os.Getenv(EnvVarsPrefix + "YAML_CONFIG_FILE")

	if cfp == "" {
		cfp = DefaultYAMLConfigFilePath
	}

	return EnvVars{
		ConfigFile: cfp,
	}
}
