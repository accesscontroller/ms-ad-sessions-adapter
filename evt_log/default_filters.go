// +build windows

package evtlog

import (
	wintypes "gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types"
)

// GetWinAuthEventsFilter returns Filter pattern set to filter only Windows
// Logon/Logoff events as described:
// https://www.ultimatewindowssecurity.com/securitylog/book/page.aspx?spid=chapter5.
func GetWinAuthEventsFilter() *wintypes.Filter {
	return &wintypes.Filter{
		IncludePatterns: wintypes.FilterRecord{
			System: wintypes.FilterRecordSystemPart{
				EventID: "4624|4634",
			},
		},
		ExcludePatterns: wintypes.FilterRecord{
			EventData: map[string]string{
				"IpAddress": "-",
			},
		},
	}
}
