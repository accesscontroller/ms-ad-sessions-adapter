// +build windows

/*
Windows Event Log reader wrapper based on github.com/0xrawsec/golang-win32/win32/wevtapi.

The wrapper is intended to provide as stable API as possible for ms-ad-sessions-adapter.
It implements only Event based log reading from Windows Event Log.
*/

package evtlog

import (
	"fmt"

	"github.com/0xrawsec/golang-win32/win32/wevtapi"
	"github.com/sirupsen/logrus"
	wintypes "gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types"
)

// EvtLog a Windows Event Log subscriber.
type EvtLog struct {
	prov             *wevtapi.PushEventProvider
	winEvtLogSources []string
	filter           wintypes.EventFilterEvaluator

	logger logrus.FieldLogger
}

// New creates a new Windows Event Log subscriber.
func New(winEvtLogSources []string, logger *logrus.Logger) (EvtLog, error) {
	return EvtLog{
		winEvtLogSources: winEvtLogSources,
		filter:           func(e *wintypes.RawEvent) bool { return true },
		logger:           logger.WithField("package", "evtlog"),
	}, nil
}

// SetFilter compiles and sets Filter for Events.
func (e *EvtLog) SetFilter(f *wintypes.Filter) error {
	eval, err := wintypes.ComputeEvaluator(f)
	if err != nil {
		return fmt.Errorf("can not compile SetFilter evaluator: %w", err)
	}

	e.filter = eval

	return nil
}

// FetchEvents starts Windows Event Log reading.
func (e *EvtLog) FetchEvents() (<-chan wintypes.Event, error) {
	var (
		prov    = wevtapi.NewPushEventProvider()
		outFlow = make(chan wintypes.Event)
	)

	e.prov = prov

	go func() {
		for evt := range prov.FetchEvents(e.winEvtLogSources,
			wevtapi.EvtSubscribeToFutureEvents) {
			// ToDo: it is possible to exclude wevtapi.XMLEvent copy to Event
			// by filtering before the copy operation.
			raw := &wintypes.RawEvent{
				XMLEvent: *evt,
			}

			// Filter.
			if !e.filter(raw) {
				continue
			}

			// Parse and send.
			pEvt, err := wintypes.ParseWinEvent(raw)
			if err != nil {
				e.logger.WithFields(logrus.Fields{
					"RawEvent": raw,
					"error":    err,
				}).Error("can not parse RawEvent")

				continue
			}

			outFlow <- *pEvt
		}

		// Close after all
		close(outFlow)
	}()

	return outFlow, nil
}

// Stop stops Windows Event Log reading and releases resources.
func (e *EvtLog) Stop() {
	e.prov.Stop()
}
