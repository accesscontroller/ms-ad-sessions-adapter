module gitlab.com/accesscontroller/ms-ad-sessions-adapter/evt_log

go 1.14

require (
	github.com/0xrawsec/golang-win32 v1.0.3
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types v0.0.0-20200719093937-55bc959410d1
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
)
