package main

import (
	"crypto/tls"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/accesscontroller/cclient.v1"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/ldapclient"
)

// adapterConnections contains set of Connections to External Services.
type adapterConnections struct {
	logger    *logrus.Logger
	LDAP      *ldapclient.Client
	APIClient *cclient.CClient
}

// connectExternalServices connects to required external services.
func connectExternalServices(config *Config) (adapterConnections, error) {
	// Open Log stream.
	logStream, err := openLogOutput(config.Log.LogOutput)
	if err != nil {
		return adapterConnections{}, fmt.Errorf("can not open Log Stream: %w", err)
	}

	// Make API Server Client.
	apiClient, err := cclient.NewCustomConfig(cclient.ServerConfig{
		URL:     config.APIServer.URL,
		Timeout: config.APIServer.RequestTimeout,
		TLS: &tls.Config{
			Certificates:       []tls.Certificate{config.APIServer.ClientCerts},
			RootCAs:            config.APIServer.CACert,
			InsecureSkipVerify: config.APIServer.InsecureConnection, // nolint:gosec
		},
	})
	if err != nil {
		return adapterConnections{}, fmt.Errorf("can not create API Client connection: %w", err)
	}

	// Open Connection to LDAP Server.
	ldapCl, err := ldapclient.NewDefaultMSAD(
		config.UsersGroups.LDAPSettings.Server,
		config.UsersGroups.LDAPSettings.Username,
		config.UsersGroups.LDAPSettings.Password,
		config.UsersGroups.LDAPSettings.BaseDN,
		config.UsersGroups.LDAPSettings.TLSMode,
		config.UsersGroups.LDAPSettings.TLSConfig,
		config.UsersGroups.UsersGroupsSource)
	if err != nil {
		return adapterConnections{}, fmt.Errorf(
			"can not connect to LDAP Server %q: %w",
			config.UsersGroups.LDAPSettings.Server, err)
	}

	return adapterConnections{
		logger:    createLogger(logStream, config.Log.LogLevel),
		APIClient: apiClient,
		LDAP:      ldapCl,
	}, nil
}
