module gitlab.com/accesscontroller/ms-ad-sessions-adapter

go 1.14

require (
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200719141450-e53abe583345
	gitlab.com/accesscontroller/cclient.v1 v0.0.0-20200720174159-3ee9dbf0f479
	gitlab.com/accesscontroller/lib/syncwriter v0.0.0-20200202094040-4893992d6155
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/evt_log v0.0.0-20200720183444-91483195477a
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/ldapclient v0.0.0-20200720183444-91483195477a
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/sessions v0.0.0-20200720183444-91483195477a
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/users-groups v0.0.0-20200720183444-91483195477a
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/types v0.0.0-20200720183444-91483195477a // indirect
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types v0.0.0-20200720183444-91483195477a
	gopkg.in/yaml.v2 v2.3.0
)

replace gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/sessions => ./synchronizer/sessions
