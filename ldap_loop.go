package main

import (
	"time"

	"github.com/sirupsen/logrus"
	usersgroups "gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/users-groups"
)

func startLDAPPollLoop(logger *logrus.Logger, synch *usersgroups.Syncer, interval time.Duration) {
	var iLog = logger.WithField("process", "sync/users-groups")

	for {
		if err := synch.SyncGroups(); err != nil {
			iLog.Errorf("Error sync users-groups: %s", err)
			time.Sleep(interval)

			continue
		}

		iLog.Info("Success sync users-groups")
		time.Sleep(interval)
	}
}
