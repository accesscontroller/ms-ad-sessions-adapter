package ldapclient

import (
	"errors"
)

var (
	ErrUnsupportedSchema    = errors.New("unsupported schema type given")
	ErrNotFound             = errors.New("requested resource not found")
	ErrInvalidTLSSettings   = errors.New("invalid TLS mode")
	ErrInvalidLDAPSchema    = errors.New("invalid LDAP schema")
	ErrEmptyUsersListGiven  = errors.New("empty users list given")
	ErrEmptyGroupsListGiven = errors.New("empty groups list given")
)
