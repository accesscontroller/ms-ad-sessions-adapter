module gitlab.com/accesscontroller/ms-ad-sessions-adapter/ldapclient

go 1.13

require (
	github.com/go-asn1-ber/asn1-ber v1.5.1 // indirect
	github.com/go-ldap/ldap/v3 v3.2.3
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200719141450-e53abe583345
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/types v0.0.0-20200719093937-55bc959410d1
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899 // indirect
)
