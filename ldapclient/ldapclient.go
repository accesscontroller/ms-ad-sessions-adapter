/*
Implements LDAP server client with some event loops.
The package can load to API server LDAP users and groups (using LDAP filter).

Package allows usage of multiple LDAP servers for high availability.
*/
package ldapclient

import (
	"crypto/tls"
	"time"

	"github.com/go-ldap/ldap/v3"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/types"
)

type SchemaType int

const (
	SchemaTypeActiveDirectory SchemaType = 1
	SafeMaxResultPerLDAPPage  uint32     = 500
)

// ParseSchemaType parses string SchemaType to value.
func ParseSchemaType(s string) (SchemaType, error) {
	switch s {
	case "ActiveDirectory":
		return SchemaTypeActiveDirectory, nil

	default:
		return 0, ErrInvalidLDAPSchema
	}
}

const (
	// cnFieldNameMSAD       = "cn"
	sAMAccountNameMSAD string = "sAMAccountName"
	// dnFieldNameMSAD       = "distinguishedName"
	memberOfFieldNameMSAD string = "memberOf"
	memberOfFilterMSAD    string = `(` + memberOfFieldNameMSAD + `=%s)`

	msADGroupLookupQuery        string = "(&(objectCategory=group)(" + sAMAccountNameMSAD + "=%s))"
	msADUserLookupQuery         string = "(&(objectCategory=user)(" + sAMAccountNameMSAD + "=%s))"
	msADGroupMembersLookupQuery string = "(&(objectCategory=user)(" + memberOfFieldNameMSAD + "=%s))"
)

// TLSMode mode of TLS connection to LDAP server.
type TLSMode int

// ParseTLSMode parses string to TLSMode. Returns error in case of unsupported mode.
func ParseTLSMode(tm string) (TLSMode, error) {
	switch tm {
	case "NoTLS":
		return NoTLS, nil
	case "TLS":
		return TLS, nil
	case "StartTLS":
		return StartTLS, nil

	default:
		return 0, ErrInvalidTLSSettings
	}
}

const (
	NoTLS    TLSMode = 1
	TLS      TLSMode = 2
	StartTLS TLSMode = 3
)

// Client - LDAP client.
type Client struct {
	server, username, password, baseDN string
	iTLSMode                           TLSMode
	iTLSConfig                         *tls.Config
	schemaType                         SchemaType
	maxResultLength                    uint32
	iUGSource                          storage.ResourceNameT
	_LDAPTimeout                       time.Duration
	_LDAPQueryTimeoutSec               int
}

// New creates a new default client.
// server must contain hostname:port_number.
func New(server, username, password, baseDN string, mode TLSMode, config *tls.Config,
	sourceName storage.ResourceNameT, tp SchemaType,
	_LDAPTimeout time.Duration, _LDAPQueryTimeoutSec int) (*Client, error) {
	// Validate schema type
	if tp != SchemaTypeActiveDirectory {
		return nil, ErrUnsupportedSchema
	}

	// Use defult TLS config
	if config == nil {
		config = &tls.Config{}
	}

	cl := &Client{
		server:               server,
		username:             username,
		password:             password,
		iUGSource:            sourceName,
		baseDN:               baseDN,
		schemaType:           tp,
		maxResultLength:      SafeMaxResultPerLDAPPage,
		iTLSMode:             mode,
		iTLSConfig:           config,
		_LDAPTimeout:         _LDAPTimeout,
		_LDAPQueryTimeoutSec: _LDAPQueryTimeoutSec,
	}

	// Test connection
	if err := cl.wrapConnection(func(conn *ldap.Conn) error {
		return nil
	}); err != nil {
		return nil, err
	}

	return cl, nil
}

// NewDefaultMSAD initializes a new Client with Microsoft AD default settings.
func NewDefaultMSAD(server, username, password, baseDN string,
	mode TLSMode, config *tls.Config, sourceName storage.ResourceNameT) (*Client, error) {
	const (
		defaultConnTimeout     = 30 * time.Second
		defaultQueryTimeoutSec = 30
	)

	return New(server, username, password, baseDN, mode, config, sourceName,
		SchemaTypeActiveDirectory, defaultConnTimeout, defaultQueryTimeoutSec)
}

// LoadGroup loads one group from LDAP server.
func (c *Client) LoadGroup(name storage.ResourceNameT, withUsers bool) (*types.GroupWUsers, error) {
	switch c.schemaType {
	case SchemaTypeActiveDirectory:
		g, err := c.loadGroupMSAD(name, withUsers)
		if err != nil {
			return nil, err
		}

		return g, nil

	default:
		return nil, ErrUnsupportedSchema
	}
}

// LoadGroups loads groups from LDAP server by names. If names is empty - loads all groups.
func (c *Client) LoadGroups(
	names []storage.ResourceNameT, withUsers bool) (map[storage.ResourceNameT]*types.GroupWUsers, error) {
	switch c.schemaType {
	case SchemaTypeActiveDirectory:
		return c.loadGroupsMSAD(names, withUsers)

	default:
		return nil, ErrUnsupportedSchema
	}
}

// LoadUser loads one user from LDAP by user name.
func (c *Client) LoadUser(name storage.ResourceNameT, withGroups bool) (*types.UserWGroups, error) {
	switch c.schemaType {
	case SchemaTypeActiveDirectory:
		u, err := c.loadUserMSAD(name, withGroups)
		if err != nil {
			return nil, err
		}

		return u, nil
	default:
		return nil, ErrUnsupportedSchema
	}
}

// LoadUsers loads users from LDAP by names.
func (c *Client) LoadUsers(
	names []storage.ResourceNameT, withGroups bool) (map[storage.ResourceNameT]*types.UserWGroups, error) {
	switch c.schemaType {
	case SchemaTypeActiveDirectory:
		return c.loadUsersMSAD(names, withGroups)
	default:
		return nil, ErrUnsupportedSchema
	}
}

// FilterUserGroupsMembership returns subset of given userNames which are
// members of at least one of groups in groupNames.
func (c *Client) FilterUserGroupsMembership(userNames, groupNames []storage.ResourceNameT) (map[storage.ResourceNameT]bool, error) {
	switch c.schemaType {
	case SchemaTypeActiveDirectory:
		return c.filterUsersByGrpsMSAD(userNames, groupNames)
	default:
		return nil, ErrUnsupportedSchema
	}
}

// IsMemberOf checks if a given User is a member of at least one of given groups
func (c *Client) IsMemberOf(user storage.ResourceNameT, groups ...storage.ResourceNameT) (bool, error) {
	switch c.schemaType {
	case SchemaTypeActiveDirectory:
		return c.isMemberOfMSAD(user, groups)
	default:
		return false, ErrUnsupportedSchema
	}
}

// wrapConnection serves as a connection wrapper for LDAP server.
func (c *Client) wrapConnection(fn func(с *ldap.Conn) error) error {
	// Connect
	var l *ldap.Conn

	switch c.iTLSMode {
	case NoTLS:
		var err error

		l, err = ldap.Dial("tcp", c.server)
		if err != nil {
			return err
		}
	case TLS:
		var err error

		l, err = ldap.DialTLS("tcp", c.server, c.iTLSConfig)
		if err != nil {
			return err
		}
	case StartTLS:
		var err error

		l, err = ldap.Dial("tcp", c.server)
		if err != nil {
			return err
		}

		err = l.StartTLS(c.iTLSConfig)
		if err != nil {
			return err
		}
	default:
		return ErrInvalidTLSSettings
	}

	// Authenticate
	if err := l.Bind(c.username, c.password); err != nil {
		return err
	}

	l.SetTimeout(c._LDAPTimeout)

	defer l.Close()

	return fn(l)
}
