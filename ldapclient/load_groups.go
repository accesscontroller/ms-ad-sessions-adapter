package ldapclient

import (
	"fmt"
	"strings"

	"github.com/go-ldap/ldap/v3"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/types"
)

// loadGroupMSAD loads one group from LDAP server using Microsoft Active Directory scheme.
func (c *Client) loadGroupMSAD(name storage.ResourceNameT, withUsers bool) (*types.GroupWUsers, error) {
	const (
		expectedResultLength = 1
	)

	var (
		users []storage.ResourceNameT
		group *storage.ExternalGroupV1
	)

	if err := c.wrapConnection(func(cl *ldap.Conn) error {
		// Make group query.
		req := ldap.NewSearchRequest(c.baseDN, ldap.ScopeWholeSubtree,
			ldap.NeverDerefAliases, expectedResultLength,
			c._LDAPQueryTimeoutSec, false, fmt.Sprintf(msADGroupLookupQuery, name),
			nil, nil)

		// Load a group.
		ldapGrs, err := cl.Search(req)
		if err != nil {
			return err
		}

		// Must return exactly 1 group.
		if len(ldapGrs.Entries) != expectedResultLength {
			return ErrNotFound
		}

		gr := ldapGrs.Entries[0]

		// Make group.
		group = storage.NewExternalGroupV1(storage.ResourceNameT(gr.DN), c.iUGSource)

		// Load users for group.
		if withUsers {
			usrs, err := c.loadUsersForGroupMSAD(cl, gr.DN)
			if err != nil {
				return err
			}

			users = usrs
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return &types.GroupWUsers{
		Group: group,
		Users: users,
	}, nil
}

// loadGroupsMSAD loads groups from LDAP server using Microsoft Active Directory scheme.
// If names == nil or empty - returns ALL groups. Key in returning map is group's name.
func (c *Client) loadGroupsMSAD(
	names []storage.ResourceNameT, withUsers bool) (map[storage.ResourceNameT]*types.GroupWUsers, error) {
	var stGrps map[storage.ResourceNameT]*types.GroupWUsers

	if err := c.wrapConnection(func(cl *ldap.Conn) error {
		// Get groups by names.
		ldapGrps, err := queryGroups(names, cl, c.maxResultLength, c.baseDN, c._LDAPQueryTimeoutSec)
		if err != nil {
			return err
		}

		// Make groups.
		resExGrps := make(map[storage.ResourceNameT]*types.GroupWUsers, len(ldapGrps.Entries))
		groupDNs := make(map[storage.ResourceNameT]string, len(ldapGrps.Entries))

		for _, v := range ldapGrps.Entries {
			nm := storage.ResourceNameT(v.GetAttributeValue(sAMAccountNameMSAD))
			resExGrps[nm] = &types.GroupWUsers{
				Group: storage.NewExternalGroupV1(nm, c.iUGSource),
			}

			groupDNs[nm] = v.DN
		}

		// Load users.
		if withUsers {
			users, err := c.loadUsersForGroupsMSAD(cl, groupDNs)
			if err != nil {
				return err
			}

			for k, v := range resExGrps {
				us, ok := users[k]
				if !ok {
					return fmt.Errorf(
						"key %q was not found in map %+v. Source code error", k, users)
				}

				v.Users = us
			}
		}

		// Result
		stGrps = resExGrps

		return nil
	}); err != nil {
		return nil, err
	}

	return stGrps, nil
}

// loadUsersForGroupsMSAD loads users to groups.
func (c *Client) loadUsersForGroupsMSAD(cl *ldap.Conn,
	groupDNs map[storage.ResourceNameT]string) ( // nolint: stylecheck,golint here is groupDistinguishedNames
	map[storage.ResourceNameT][]storage.ResourceNameT, error) {
	res := make(map[storage.ResourceNameT][]storage.ResourceNameT, len(groupDNs))

	for i, v := range groupDNs {
		// Make filter to get users.
		usersFlt := fmt.Sprintf(msADGroupMembersLookupQuery, v)

		uReq := ldap.NewSearchRequest(
			c.baseDN, ldap.ScopeWholeSubtree,
			ldap.NeverDerefAliases, int(c.maxResultLength),
			c._LDAPQueryTimeoutSec, false,
			usersFlt, []string{sAMAccountNameMSAD},
			[]ldap.Control{ldap.NewControlPaging(c.maxResultLength)})

		uReq.SizeLimit = int(c.maxResultLength)

		// Load users.
		sRes, err := cl.Search(uReq)
		if err != nil {
			return nil, err
		}

		// Add them to group.
		usrL := make([]storage.ResourceNameT, 0, len(sRes.Entries))

		for _, v := range sRes.Entries {
			usrL = append(usrL, storage.ResourceNameT(v.GetAttributeValue(sAMAccountNameMSAD)))
		}

		res[i] = usrL
	}

	return res, nil
}

// loadUsersForGroupMSAD loads all users from LDAP for given group DN.
func (c *Client) loadUsersForGroupMSAD(cl *ldap.Conn, DN string) ([]storage.ResourceNameT, error) {
	// Make users lookup query
	req := ldap.NewSearchRequest(c.baseDN, ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases, int(c.maxResultLength), c._LDAPQueryTimeoutSec, false,
		fmt.Sprintf("(&(objectCategory=user)(%s=%s))", memberOfFieldNameMSAD, DN),
		[]string{sAMAccountNameMSAD}, []ldap.Control{ldap.NewControlPaging(c.maxResultLength)})

	req.SizeLimit = int(c.maxResultLength)

	// Load users.
	ldapUsrs, err := cl.Search(req)
	if err != nil {
		return nil, err
	}

	// Make result.
	usrs := make([]storage.ResourceNameT, 0, len(ldapUsrs.Entries))
	for _, v := range ldapUsrs.Entries {
		usrs = append(usrs, storage.ResourceNameT(v.GetAttributeValue(sAMAccountNameMSAD)))
	}

	return usrs, nil
}

// queryGroups returns groups using their names from LDAP.
func queryGroups(names []storage.ResourceNameT, cl *ldap.Conn,
	maxResultLength uint32, baseDN string, queryTimeLimitSec int) (*ldap.SearchResult, error) {
	// Make groups filtering expression
	var grFilter string

	// No names - all groups.
	if len(names) != 0 {
		nms := make([]interface{}, len(names))
		for i := range names {
			nms[i] = names[i]
		}

		groupsSubFlt := fmt.Sprintf(strings.Repeat("("+sAMAccountNameMSAD+"=%s)", len(nms)), nms...)
		grFilter = fmt.Sprintf("(&(objectCategory=group)(|%s))", groupsSubFlt)
	} else {
		grFilter = "(objectCategory=group)"
	}

	// Query for groups.
	reqGr := ldap.NewSearchRequest(baseDN, ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases, int(maxResultLength), queryTimeLimitSec, false,
		grFilter, []string{sAMAccountNameMSAD},
		[]ldap.Control{ldap.NewControlPaging(maxResultLength)})

	reqGr.SizeLimit = int(maxResultLength)

	ldapGrps, err := cl.Search(reqGr)
	if err != nil {
		return nil, err
	}

	return ldapGrps, nil
}
