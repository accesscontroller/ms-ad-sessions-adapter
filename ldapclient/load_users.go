package ldapclient

import (
	"fmt"
	"strings"

	"github.com/go-ldap/ldap/v3"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/types"
)

// loadUserMSAD loads one user from LDAP server using Microsoft Active Directory scheme.
func (c *Client) loadUserMSAD(name storage.ResourceNameT, withGroups bool) (*types.UserWGroups, error) {
	const expectedUsersCount int = 1 // LDAP must return exactly one user for one name.

	var stEU types.UserWGroups

	if err := c.wrapConnection(func(cl *ldap.Conn) error {
		// Query.
		req := ldap.NewSearchRequest(c.baseDN, ldap.ScopeWholeSubtree,
			ldap.NeverDerefAliases, int(c.maxResultLength), c._LDAPQueryTimeoutSec, false,
			fmt.Sprintf(msADUserLookupQuery, name),
			[]string{memberOfFieldNameMSAD, sAMAccountNameMSAD},
			[]ldap.Control{ldap.NewControlPaging(c.maxResultLength)})
		req.SizeLimit = int(c.maxResultLength)

		// Load.
		ldapUsrs, err := cl.Search(req)
		if err != nil {
			return err
		}

		if len(ldapUsrs.Entries) != expectedUsersCount {
			return ErrNotFound
		}

		ldapUsr := ldapUsrs.Entries[0]

		// Make res.
		usr := storage.NewExternalUserV1(
			storage.ResourceNameT(ldapUsr.GetAttributeValue(sAMAccountNameMSAD)),
			c.iUGSource)

		// Load groups.
		if withGroups {
			// Groups.
			memberOf := ldapUsr.GetAttributeValues(memberOfFieldNameMSAD)

			grps := make([]storage.ResourceNameT, 0, len(memberOf))

			for _, gName := range memberOf {
				grps = append(grps, storage.ResourceNameT(gName))
			}

			stEU.Groups = grps
		}

		// Result.
		stEU.User = usr

		return nil
	}); err != nil {
		return nil, err
	}

	return &stEU, nil
}

// loadUsersMSAD loads users from LDAP server using Microsoft Active Directory scheme.
// If names == nil or empty slice - returns ALL users. Key in map is user's name.
func (c *Client) loadUsersMSAD(names []storage.ResourceNameT, withGroups bool) (map[storage.ResourceNameT]*types.UserWGroups, error) {
	stEUs := make(map[storage.ResourceNameT]*types.UserWGroups)

	if err := c.wrapConnection(func(cl *ldap.Conn) error {

		// Filter LDAP.
		var usrsFilter string

		// No names - all users.
		if len(names) != 0 {
			nms := make([]interface{}, len(names))
			for i := range names {
				nms[i] = names[i]
			}

			usersSubFlt := fmt.Sprintf(strings.Repeat("("+sAMAccountNameMSAD+"=%s)", len(nms)), nms...)
			usrsFilter = fmt.Sprintf("(&(objectCategory=user)(|%s))", usersSubFlt)
		} else {
			usrsFilter = "(objectCategory=user)"
		}

		// Query.
		req := ldap.NewSearchRequest(c.baseDN, ldap.ScopeWholeSubtree,
			ldap.NeverDerefAliases, 1, 0, false,
			usrsFilter, []string{memberOfFieldNameMSAD, sAMAccountNameMSAD},
			[]ldap.Control{ldap.NewControlPaging(c.maxResultLength)})
		req.SizeLimit = int(c.maxResultLength)

		// Load.
		ldapUsrs, err := cl.Search(req)
		if err != nil {
			return err
		}

		for _, v := range ldapUsrs.Entries {
			var usWGroups types.UserWGroups

			// Make res.
			usWGroups.User = storage.NewExternalUserV1(
				storage.ResourceNameT(v.GetAttributeValue(sAMAccountNameMSAD)), c.iUGSource)

			// Load groups.
			if withGroups {
				// Groups.
				memberOf := v.GetAttributeValues(memberOfFieldNameMSAD)

				grps := make([]storage.ResourceNameT, 0, len(memberOf))

				for _, gName := range memberOf {
					grps = append(grps, storage.ResourceNameT(gName))
				}

				usWGroups.Groups = grps
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return stEUs, nil
}

func (c *Client) filterUsersByGrpsMSAD(userNames, groupNames []storage.ResourceNameT) (
	map[storage.ResourceNameT]bool, error) {
	if len(userNames) == 0 {
		return nil, ErrEmptyUsersListGiven
	}

	if len(groupNames) == 0 {
		return nil, ErrEmptyGroupsListGiven
	}

	result := make(map[storage.ResourceNameT]bool, len(userNames))

	if err := c.wrapConnection(func(conn *ldap.Conn) error {
		ldapGrps, err := queryGroups(groupNames, conn, c.maxResultLength, c.baseDN, c._LDAPQueryTimeoutSec)
		if err != nil {
			return err
		}

		var queryBuilder strings.Builder
		queryBuilder.WriteString("(&(objectCategory=user)(|")

		san := "(" + sAMAccountNameMSAD + "=%s)"
		for i := range userNames {
			queryBuilder.WriteString(fmt.Sprintf(san, userNames[i]))
		}
		queryBuilder.WriteString(")(|")

		for _, v := range ldapGrps.Entries {
			queryBuilder.WriteString(fmt.Sprintf(memberOfFilterMSAD, v.DN))
		}
		queryBuilder.WriteString("))")

		req := ldap.NewSearchRequest(c.baseDN, ldap.ScopeWholeSubtree,
			ldap.NeverDerefAliases, 1, 0, false,
			queryBuilder.String(), []string{sAMAccountNameMSAD},
			[]ldap.Control{ldap.NewControlPaging(c.maxResultLength)})
		req.SizeLimit = int(c.maxResultLength)

		res, err := conn.Search(req)
		if err != nil {
			return err
		}

		for _, v := range res.Entries {
			result[storage.ResourceNameT(v.GetAttributeValue(sAMAccountNameMSAD))] = true
		}

		return nil
	}); err != nil {
		return nil, err
	}
	return result, nil
}

func (c *Client) isMemberOfMSAD(user storage.ResourceNameT, groups []storage.ResourceNameT) (bool, error) {
	// Check for obvious error.
	if len(groups) == 0 {
		return false, ErrEmptyGroupsListGiven
	}

	var isMember bool

	if err := c.wrapConnection(func(conn *ldap.Conn) error {
		var err error

		isMember, err = queryIsUserMemberOf(user, groups, conn, c.maxResultLength, c.baseDN, c._LDAPQueryTimeoutSec)
		if err != nil {
			return fmt.Errorf("can not check isMemberOf: %w", err)
		}

		return nil
	}); err != nil {
		return false, err
	}

	return isMember, nil
}

func queryIsUserMemberOf(name storage.ResourceNameT, groups []storage.ResourceNameT,
	cl *ldap.Conn, maxResultLength uint32, baseDN string, queryTimeLimitSec int) (bool, error) {
	const errPrefix string = "can not queryUserGroups: %w"

	// Load groups.
	ldapGroups, err := queryGroups(groups, cl, maxResultLength, baseDN, queryTimeLimitSec)
	if err != nil {
		return false, fmt.Errorf(errPrefix, err)
	}

	// Build User is a member filter.
	var (
		LDAPFilter strings.Builder
	)

	// Username filter.
	if _, err := LDAPFilter.WriteString(
		fmt.Sprintf("(&(objectCategory=user)(%s=%s)(|",
			sAMAccountNameMSAD, name)); err != nil {
		return false, fmt.Errorf(errPrefix, err)
	}

	// Groups membership filter.
	for _, ldapGroup := range ldapGroups.Entries {
		if _, err := LDAPFilter.WriteString(fmt.Sprintf(
			memberOfFilterMSAD, ldapGroup.DN)); err != nil {
			return false, fmt.Errorf(errPrefix, err)
		}
	}

	if _, err := LDAPFilter.WriteString("))"); err != nil {
		return false, fmt.Errorf(errPrefix, err)
	}

	// Query for membership.
	// Make users lookup query.
	req := ldap.NewSearchRequest(baseDN, ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases, int(maxResultLength), queryTimeLimitSec, false,
		LDAPFilter.String(), nil,
		[]ldap.Control{ldap.NewControlPaging(maxResultLength)})

	req.SizeLimit = int(maxResultLength)

	// Load user. I expect here one or nothing.
	ldapUsrs, err := cl.Search(req)
	if err != nil {
		return false, err
	}

	// If result is not empty then the given user is a member of at least one of groups.
	return len(ldapUsrs.Entries) != 0, nil
}
