package main

import (
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/accesscontroller/lib/syncwriter"
)

func openLogOutput(out string) (io.WriteCloser, error) {
	// Process common cases.
	if strings.EqualFold(out, "stdout") {
		return syncwriter.NewSyncWriteCloser(os.Stdout), nil
	}

	if strings.EqualFold(out, "stderr") {
		return syncwriter.NewSyncWriteCloser(os.Stderr), nil
	}

	// Open file.
	fd, err := os.OpenFile(out, os.O_CREATE|os.O_APPEND|os.O_WRONLY|os.O_SYNC, 0600)
	if err != nil {
		return nil, fmt.Errorf("can not open Log file %q: %w", out, err)
	}

	return syncwriter.NewSyncWriteCloser(fd), nil
}

func createLogger(writer io.Writer, level logrus.Level) *logrus.Logger {
	var logger = logrus.New()

	logger.SetLevel(level)
	logger.SetFormatter(&logrus.JSONFormatter{})
	logger.SetOutput(writer)

	return logger
}
