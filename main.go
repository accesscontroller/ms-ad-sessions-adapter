package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/ldapclient"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/sessions"
	usersgroups "gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/users-groups"
	wintypes "gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types"
)

func main() {
	// Load Environment Variables.
	envConf := parseEnv()

	// Open Config.
	yamlData, err := ioutil.ReadFile(envConf.ConfigFile)
	if err != nil {
		log.Fatalln(err)
	}

	// Load Configuration.
	config, err := parseYaml(yamlData)
	if err != nil {
		log.Fatalln(err)
	}

	// Connect to external Services.
	externalServices, err := connectExternalServices(config)
	if err != nil {
		log.Fatalln(err)
	}

	// Start Users and Groups synchronization if enabled.
	if config.UsersGroups.Enabled {
		// Create Users and Groups synchronization client.
		ugSyncer, err := usersgroups.New(config.InstanceName, prometheus.DefaultRegisterer,
			config.UsersGroups.UsersGroupsSource,
			externalServices.APIClient, externalServices.APIClient,
			externalServices.LDAP, externalServices.LDAP,
			config.UsersGroups.GroupNames)
		if err != nil {
			log.Fatalln(fmt.Errorf("can not create usersgroups synchronization client: %w", err))
		}

		go startLDAPPollLoop(externalServices.logger, ugSyncer, config.UsersGroups.Interval)
	}

	// Sessions synchronization.
	go startSessionsLoop(config,
		getSessionsSkipper(externalServices.LDAP,
			config.UsersGroups.LDAPSettings.Username,
			config.UsersGroups.GroupNames),
		externalServices.APIClient,
		externalServices.logger, prometheus.DefaultRegisterer)

	// Start metrics server.
	http.Handle("/metrics", promhttp.Handler())

	log.Fatalln(http.ListenAndServe(":8080", nil))
}

func getSessionsSkipper(cl *ldapclient.Client,
	ldapUser string, groups []storage.ResourceNameT) sessions.EventSkipper {
	return func(evt *wintypes.Event) bool {
		// Skip a user which is used for LDAP queries so as not to cause
		// authentication loop by generating more authentication
		// events from the user.
		if evt.EventData.TargetUserName == ldapUser {
			return true
		}

		// Check groups membership.
		member, err := cl.IsMemberOf(storage.ResourceNameT(evt.EventData.TargetUserName), groups...)
		if err != nil {
			return false
		}

		// Skip all not-members.
		return !member
	}
}
