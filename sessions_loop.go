package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	evtlog "gitlab.com/accesscontroller/ms-ad-sessions-adapter/evt_log"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/sessions"
)

func startSessionsLoop(config *Config, sk sessions.EventSkipper,
	apiClient sessions.APISessionCRUD,
	logger *logrus.Logger, reg prometheus.Registerer) error {
	// Create a new synchronizer.
	sessionsSyncer, err := sessions.New(config.InstanceName,
		apiClient, config.Sessions.SessionsSource, config.Sessions.SessionTimeout,
		reg, logger)
	if err != nil {
		return err
	}

	// Skip sessions.
	sessionsSyncer.SetSkipper(sk)

	// Create Windows Event Log stream.
	var evtLog = logger.WithField("process", "evt_log")

	evtStream, err := evtlog.New(config.Sessions.WindowsEventLogs, evtLog.Logger)
	if err != nil {
		return err
	}

	// Filter only Windows Logon and Logoff events.
	if err := evtStream.SetFilter(evtlog.GetWinAuthEventsFilter()); err != nil {
		return err
	}

	// Get the Windows Event Log Stream.
	evtFlow, err := evtStream.FetchEvents()
	if err != nil {
		return err
	}

	// Prepare logger.
	var iLog = logger.WithField("process", "sync/sessions")

	// Start Close Sessions Loop.
	sessionsSyncer.StartCloseLoop(config.Sessions.CloseSessionsInterval)

	// Start sessions synchronization.
	for {
		event := <-evtFlow

		if err := sessionsSyncer.HandleSessionEvent(&event); err != nil {
			iLog.WithFields(logrus.Fields{
				"error": err,
			}).Error("Error HandleSessionEvent")

			continue
		}

		iLog.Info("Success HandleSessionEvent")
	}
}
