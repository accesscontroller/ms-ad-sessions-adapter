package sessions

import (
	"fmt"
	"time"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

/* closeOldSessions closes stale sessions.
The method of closing just stale sessions instead of reacting on 4634 Windows Security Event
is in a fact that when a Client PC connects to Microsoft Active Directory controller
to either authenticate a User or update (load) GPO definitions the MS AD controller
issues 4624 event - logon. Then the Client PC loads all the required data and disconnects
which causes 4634 event - logoff while the User is actually authorized on the Client PC.
It is possible and recommended to tune the sessionTimeout so as to reflect GPO update interval
which is usually below 90 minutes.
*/
func closeOldSessions(source storage.ResourceNameT,
	sessionsGetter APISessionGetter, sessionCloser APISessionCloser,
	sessionTimeout time.Duration) (int, error) {
	var isClosed = false

	// Load sessions.
	activeSessions, err := sessionsGetter.GetExternalUserSessions(
		source, []storage.ExternalUserSessionFilterV1{
			{
				Closed: &isClosed,
			},
		})
	if err != nil {
		return 0, fmt.Errorf("can not load sessions: %w", err)
	}

	// Close sessions.
	var (
		staleTS     = time.Now().Add(-sessionTimeout)
		closedCount int
	)

	for i := range activeSessions {
		ps := &activeSessions[i]

		if ps.Data.OpenTimestamp == nil {
			return closedCount, fmt.Errorf("can not close session %q:%q: %w",
				ps.Metadata.ExternalSource.SourceName, ps.Metadata.Name, ErrNilOpenTimestamp)
		}

		if ps.Data.OpenTimestamp.Before(staleTS) {
			// Close.
			if err := sessionCloser.CloseExternalUserSession(ps); err != nil {
				return closedCount, fmt.Errorf("can not close session %q:%q: %w",
					ps.Metadata.ExternalSource.SourceName, ps.Metadata.Name, err)
			}

			closedCount++
		}
	}

	return closedCount, nil
}
