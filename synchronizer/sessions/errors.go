package sessions

import "errors"

var (
	ErrNilInterface     = errors.New("nil interface given")
	ErrParseEventIP     = errors.New("can not parse Event IP")
	ErrNilOpenTimestamp = errors.New("unexpected nil ExternalUserSessionV1#Data#OpenTimestamp")
)
