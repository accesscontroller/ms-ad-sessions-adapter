module gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/sessions

go 1.13

require (
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200719141450-e53abe583345
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types v0.0.0-20200720183444-91483195477a
	golang.org/x/sys v0.0.0-20200720211630-cb9d2d5c5666 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
