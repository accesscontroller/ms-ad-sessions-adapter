package sessions

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// APIErrorStatusGetter defines interface which returns API error statuses.
type APIErrorStatusGetter interface {
	// IsResourceNotFound returns true if error represents any resource not found error.
	IsResourceNotFound() bool
}

// APISessionOpener defines an interface which opens one ExternalUserSessionV1.
type APISessionOpener interface {
	// OpenExternalUserSession opens one ExternalUserSessionV1.
	OpenExternalUserSession(_ *storage.ExternalUserSessionV1, loadOpened bool) (*storage.ExternalUserSessionV1, error)
}

// APISessionCloser defines an interface which closes one ExternalUserSessionV1.
type APISessionCloser interface {
	// CloseExternalUserSession closes one ExternalUserSessionV1.
	CloseExternalUserSession(*storage.ExternalUserSessionV1) error
}

// APISessionGetter defines an interface which loads either one ExternalUserSessionV1
// or many filtered ExternalUserSessionV1s..
type APISessionGetter interface {
	// GetExternalUserSessions loads and returns set of ExternalUserSessionV1.
	GetExternalUserSessions(source storage.ResourceNameT,
		filter []storage.ExternalUserSessionFilterV1) ([]storage.ExternalUserSessionV1, error)
}

// APISessionCRUD composite interface to perform all the actions with ExternalUserSessionV1.
type APISessionCRUD interface {
	APISessionOpener
	APISessionGetter
	APISessionCloser
}

// sessionCounter defines an interface to count ExternalUserSessionV1 changes.
type sessionCounter interface {
	// CountOpenSession increments Opened ExternalUserSessionV1 count.
	CountOpenSession() error
}

// measurer defines an interface to measure ExternalUserSessionV1 processing.
type measurer interface {
	sessionCounter

	// CountResult counts success/error ExternalUserSessionV1 update result.
	CountResult(success bool) error

	// GetDurationObserver returns a Prometheus Observer to measure execution time.
	GetDurationObserver() prometheus.Observer
}
