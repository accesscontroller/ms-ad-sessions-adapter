package sessions

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	// prometheusNamespace defines Prometheus metrics namespace.
	prometheusNamespace string = "MSADSessionsAdapter"

	// prometheusSubsystem defines Prometheus metrics subsystem.
	prometheusSubsystem string = "SessionsSynchronizer"

	// prometheusResultCounterName defines Prometheus operation result metric name.
	prometheusResultCounterName string = "Result"

	// prometheusOCCounterName defines Prometheus opened/closed sessions metric name.
	prometheusOCCounterName string = "Sessions"

	// prometheusDurationCounterName defines Prometheus operation duration metric name.
	prometheusDurationCounterName string = "Duration"
)

const (
	prometheusOperationLabelName string = "Operation"

	prometheusOperationLabelValueOpened string = "Opened"

	prometheusOperationLabelValueClosed string = "Closed"

	prometheusResultLabelName string = "Result"

	prometheusResultLabelSuccessValue string = "Success"

	prometheusResultLabelFailureValue string = "Failure"
)

type sessionMetrics struct {
	result *prometheus.CounterVec

	changes *prometheus.CounterVec

	executionTime prometheus.Gauge
}

func newSessionMetrics(instanceName string,
	reg prometheus.Registerer) (*sessionMetrics, error) {
	const errPrefix string = "can not create new sessionMetric: %w"

	// Result counter.
	result := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: prometheusNamespace,
		Subsystem: prometheusSubsystem,
		Name:      prometheusResultCounterName,
		ConstLabels: prometheus.Labels{
			"instance": instanceName,
		},
		Help: "Counts success/failure ExternalUserSessionV1 synchronization operations.",
	}, []string{prometheusResultLabelName})

	// Set init 0 values.
	for _, v := range []string{prometheusResultLabelFailureValue, prometheusResultLabelSuccessValue} {
		c, err := result.GetMetricWithLabelValues(v)
		if err != nil {
			return nil, fmt.Errorf(errPrefix, err)
		}

		c.Add(0)
	}

	// Register.
	if err := reg.Register(result); err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	// Open/Close sessions counter.
	changes := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: prometheusNamespace,
		Subsystem: prometheusSubsystem,
		Name:      prometheusOCCounterName,
		ConstLabels: prometheus.Labels{
			"instance": instanceName,
		},
		Help: "Counts opened and closed ExternalUserSessionV1 sessions.",
	}, []string{prometheusOperationLabelName})

	// Set init 0 values.
	for _, v := range []string{prometheusOperationLabelValueClosed, prometheusOperationLabelValueOpened} {
		c, err := changes.GetMetricWithLabelValues(v)
		if err != nil {
			return nil, fmt.Errorf(errPrefix, err)
		}

		c.Add(0)
	}

	// Register.
	if err := reg.Register(changes); err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	// Create duration counter.
	dur := prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: prometheusNamespace,
		Subsystem: prometheusSubsystem,
		Name:      prometheusDurationCounterName,
		ConstLabels: prometheus.Labels{
			"instance": instanceName,
		},
		Help: "Counts operations duration.",
	})

	dur.Add(0)

	// Register.
	if err := reg.Register(dur); err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	return &sessionMetrics{
		result:        result,
		changes:       changes,
		executionTime: dur,
	}, nil
}

func (s *sessionMetrics) CountOpenSession() error {
	m, err := s.changes.GetMetricWith(prometheus.Labels{
		prometheusOperationLabelName: prometheusOperationLabelValueOpened,
	})
	if err != nil {
		return fmt.Errorf("can not CountOpenSession: %w", err)
	}

	m.Inc()

	return nil
}

func (s *sessionMetrics) CountOpenSessions(c float64) error {
	m, err := s.changes.GetMetricWith(prometheus.Labels{
		prometheusOperationLabelName: prometheusOperationLabelValueOpened,
	})
	if err != nil {
		return fmt.Errorf("can not CountOpenSession: %w", err)
	}

	m.Add(c)

	return nil
}

func (s *sessionMetrics) CountCloseSession() error {
	m, err := s.changes.GetMetricWith(prometheus.Labels{
		prometheusOperationLabelName: prometheusOperationLabelValueClosed,
	})
	if err != nil {
		return fmt.Errorf("can not CountCloseSession: %w", err)
	}

	m.Inc()

	return nil
}

func (s *sessionMetrics) CountCloseSessions(c float64) error {
	m, err := s.changes.GetMetricWith(prometheus.Labels{
		prometheusOperationLabelName: prometheusOperationLabelValueClosed,
	})
	if err != nil {
		return fmt.Errorf("can not CountCloseSession: %w", err)
	}

	m.Add(c)

	return nil
}

func (s *sessionMetrics) CountResult(success bool) error {
	var lv string

	if success {
		lv = prometheusResultLabelSuccessValue
	} else {
		lv = prometheusResultLabelFailureValue
	}

	m, err := s.result.GetMetricWith(prometheus.Labels{
		prometheusResultLabelName: lv,
	})
	if err != nil {
		return fmt.Errorf("can not CountResult: %w", err)
	}

	m.Inc()

	return nil
}

func (s *sessionMetrics) GetDurationObserver() prometheus.Observer {
	return prometheus.ObserverFunc(s.executionTime.Add)
}
