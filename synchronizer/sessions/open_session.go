package sessions

import (
	"fmt"
	"net"
	"time"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	wintypes "gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types"
)

// openSession opens a new ExternalUserSessionV1 from Windows Event.
func openSession(sessionClient APISessionCRUD,
	sessionNamePrefix string, source storage.ResourceNameT,
	evt *wintypes.Event, sus SessionUserSyncer) error {
	const errPrefix string = "can not openSession: %w"

	// Parse EventData IP.
	clientIP := net.ParseIP(evt.EventData.IpAddress)
	if clientIP == nil {
		return fmt.Errorf(errPrefix, ErrParseEventIP)
	}

	// Close all sessions with given IP.
	if err := closeSessionsByIP(sessionClient, sessionClient, clientIP, source); err != nil {
		return fmt.Errorf(errPrefix, err)
	}

	// Synchronize a User.
	if err := sus(evt.EventData.TargetUserName); err != nil {
		return fmt.Errorf(errPrefix, err)
	}

	// Open a new Session.
	newSession := storage.NewExternalUserSessionV1(
		computeSessionName(evt.EventData.TargetLogonId, sessionNamePrefix), source)

	ts := time.Now()

	newSession.Data.OpenTimestamp = &ts
	newSession.Data.Hostname = evt.EventData.WorkstationName
	newSession.Data.IPAddress = net.ParseIP(evt.EventData.IpAddress)
	newSession.Data.ExternalUserName = storage.ResourceNameT(evt.EventData.TargetUserName)
	newSession.Data.ExternalUsersGroupsSourceName = source
	newSession.Metadata.ETag = 1 // Default start value.

	if _, err := sessionClient.OpenExternalUserSession(newSession, false); err != nil {
		return fmt.Errorf(errPrefix, err)
	}

	return nil
}

// closeSessionsByIP closes all sessions which have given IP address.
func closeSessionsByIP(sg APISessionGetter, sc APISessionCloser,
	clientIP net.IP, source storage.ResourceNameT) error {
	const errPrefix string = "can not closeSessionByIP: %w"

	var (
		sessionFound  bool = true  // Assume that session exists.
		closedSession bool = false // Just to put it into filter below.
	)

	// Look for an existing session with given IP.
	sessionList, err := sg.GetExternalUserSessions(source, []storage.ExternalUserSessionFilterV1{
		{
			IPAddress: clientIP,
			Closed:    &closedSession,
		},
	})
	if err != nil {
		// If error is not NotFound - return error.
		eS, ok := err.(APIErrorStatusGetter)
		if ok && eS.IsResourceNotFound() {
			sessionFound = false
		} else {
			return fmt.Errorf(errPrefix, err)
		}
	}

	// Close session.
	if sessionFound {
		for i := range sessionList {
			if err := sc.CloseExternalUserSession(&sessionList[i]); err != nil {
				return fmt.Errorf(errPrefix, err)
			}
		}
	}

	return nil
}
