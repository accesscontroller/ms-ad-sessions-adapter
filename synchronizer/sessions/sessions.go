package sessions

import (
	"fmt"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	wintypes "gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types"
)

// EventSkipper defines a function which allows skipping an Event processing.
type EventSkipper func(*wintypes.Event) bool

// DefaultSkipper defines a skipper which skips nothing.
func DefaultSkipper(e *wintypes.Event) bool {
	return false
}

// SessionUserSyncer defines a method which performs one ExternalUserV1
// update on API Server in order to have the ExternalUserV1 before a try to open
// ExternalUserSessionV1.
type SessionUserSyncer func(username string) error

// DefaultUserSyncer does nothing.
func DefaultUserSyncer(username string) error {
	return nil
}

// Sessions implements sessions synchronization from Windows Event Log records.
type Sessions struct {
	isCloseLoopActive bool

	logger *logrus.Logger

	sessionTimeout time.Duration

	sourceName storage.ResourceNameT

	instanceName string

	client APISessionCRUD

	metrics measurer

	sk  EventSkipper
	sus SessionUserSyncer

	mtx *sync.Mutex
}

// New creates a new Sessions synchronizer.
// if promMetrics is nil - uses default prometheus Registry.
func New(instanceName string,
	sessionsClient APISessionCRUD, source storage.ResourceNameT, sessionTimeout time.Duration,
	promMetrics prometheus.Registerer, logger *logrus.Logger) (*Sessions, error) {
	if sessionsClient == nil {
		return nil, fmt.Errorf("can not create New Sessions: %w", ErrNilInterface)
	}

	// Metrics config.
	var reg prometheus.Registerer

	if promMetrics == nil {
		reg = prometheus.DefaultRegisterer
	} else {
		reg = promMetrics
	}

	metrics, err := newSessionMetrics(instanceName, reg)
	if err != nil {
		return nil, fmt.Errorf("can New Sessions: %w", err)
	}

	return &Sessions{
		client:         sessionsClient,
		sourceName:     source,
		instanceName:   instanceName,
		metrics:        metrics,
		sk:             DefaultSkipper,
		sus:            DefaultUserSyncer,
		logger:         logger,
		mtx:            &sync.Mutex{},
		sessionTimeout: sessionTimeout,
	}, nil
}

// StartCloseLoop activates periodic polling for stale ExternalUserSessionV1s.
func (s *Sessions) StartCloseLoop(interval time.Duration) {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	s.isCloseLoopActive = true

	go func() {
		// Prepare logger.
		logger := s.logger.WithFields(
			logrus.Fields{
				"method": "StartCloseLoop",
			},
		)

		// Start poller.
		for {
			// Check if the loop must be active.
			if !func() bool {
				s.mtx.Lock()
				defer s.mtx.Unlock()

				return s.isCloseLoopActive
			}() {
				return
			}

			// Close sessions.
			res, err := closeOldSessions(s.sourceName, s.client, s.client, s.sessionTimeout)
			if err != nil {
				// Log result.
				logger.WithFields(logrus.Fields{
					"error":                 err,
					"closed_sessions_count": res,
				}).Error("Can not close sessions")

				// Metrics.
				if err := s.metrics.CountResult(false); err != nil {
					logger.WithFields(logrus.Fields{
						"error": err,
					}).Error("Can not count error result")

					return
				}
			} else {
				// Log result.
				logger.WithFields(
					logrus.Fields{
						"error":                 nil,
						"closed_sessions_count": res,
					},
				).Info("Closed ExternalUserSessionV1s")

				// Metrics.
				if err := s.metrics.CountResult(true); err != nil {
					logger.WithFields(logrus.Fields{
						"error": err,
					}).Error("Can not count success result")

					return
				}
			}

			time.Sleep(interval)
		}
	}()
}

// StopCloseLoop stops periodic polling for stale ExternalUserSessionV1s.
func (s *Sessions) StopCloseLoop() {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	s.isCloseLoopActive = false
}

// HandleSessionEvent handles one Event.
func (s *Sessions) HandleSessionEvent(evt *wintypes.Event) (err error) {
	// Log event.
	if s.logger.IsLevelEnabled(logrus.DebugLevel) {
		s.logger.WithFields(logrus.Fields{
			"method":       "HandleSessionEvent",
			"event_id":     evt.System.EventID,
			"domain_name":  evt.EventData.TargetDomainName,
			"account_name": evt.EventData.TargetUserName,
			"computer":     evt.System.Computer,
			"ip_address":   evt.EventData.IpAddress,
		}).Debug("Got event for processing")
	}

	// Check Skipper.
	if s.sk(evt) {
		return nil
	}

	s.mtx.Lock()
	defer func() {
		s.mtx.Unlock()

		if s.logger.IsLevelEnabled(logrus.DebugLevel) {
			s.logger.WithFields(logrus.Fields{
				"method":       "HandleSessionEvent",
				"event_id":     evt.System.EventID,
				"domain_name":  evt.EventData.TargetDomainName,
				"account_name": evt.EventData.TargetUserName,
				"computer":     evt.System.Computer,
				"ip_address":   evt.EventData.IpAddress,
				"error":        err,
			}).Debug("Got event for processing")
		}
	}()

	// Measure execution time.
	tm := prometheus.NewTimer(s.metrics.GetDurationObserver())
	defer tm.ObserveDuration()

	// Process event and measure results.
	if err := processEvent(s.metrics, s.client,
		s.instanceName, s.sourceName, evt, s.sus, s.logger); err != nil {
		if e := s.metrics.CountResult(false); e != nil {
			return e
		}

		return err
	}

	if err := s.metrics.CountResult(true); err != nil {
		return err
	}

	return nil
}

func computeSessionName(targetLogonID, prefix string) storage.ResourceNameT {
	return storage.ResourceNameT(fmt.Sprintf("%s:%s", prefix, targetLogonID))
}

func processEvent(metrics sessionCounter,
	client APISessionCRUD, sessionNamePrefix string,
	source storage.ResourceNameT, evt *wintypes.Event, sus SessionUserSyncer,
	logger *logrus.Logger) error {
	// Process Event.
	switch evt.System.EventID {
	case wintypes.LoggedOnEvent:
		// Log.
		if logger.IsLevelEnabled(logrus.DebugLevel) {
			logger.WithFields(logrus.Fields{
				"method":       "processEvent",
				"event_id":     evt.System.EventID,
				"domain_name":  evt.EventData.TargetDomainName,
				"account_name": evt.EventData.TargetUserName,
				"computer":     evt.System.Computer,
				"ip_address":   evt.EventData.IpAddress,
			}).Debug("Opened session event")
		}

		// Open session.
		if err := openSession(client,
			sessionNamePrefix, source, evt, sus); err != nil {
			return fmt.Errorf("can not open session: %w", err)
		}

		// Metrics.
		if err := metrics.CountOpenSession(); err != nil {
			return fmt.Errorf("can not CountOpenSession: %w", err)
		}

	default:
		// Skip.
		// Log.
		if logger.IsLevelEnabled(logrus.DebugLevel) {
			logger.WithFields(logrus.Fields{
				"method":       "processEvent",
				"event_id":     evt.System.EventID,
				"domain_name":  evt.EventData.TargetDomainName,
				"account_name": evt.EventData.TargetUserName,
				"computer":     evt.System.Computer,
				"ip_address":   evt.EventData.IpAddress,
			}).Debug("Unknown event")
		}

		return nil
	}

	return nil
}

// SetSkipper sets custom Skipper.
func (s *Sessions) SetSkipper(sk EventSkipper) {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	s.sk = sk
}

// SetUserSyncer sets custom SessionUserSyncer to
// use it to synchronize an ExternalUserV1 before open ExternalUserSessionV1.
func (s *Sessions) SetUserSyncer(sus SessionUserSyncer) {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	s.sus = sus
}
