package usersgroups

import "errors"

var (
	ErrLoopNotStarted = errors.New("loop has not been started yet. Can not stop")
)
