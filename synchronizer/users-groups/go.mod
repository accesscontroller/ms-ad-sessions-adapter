module gitlab.com/accesscontroller/ms-ad-sessions-adapter/synchronizer/users-groups

go 1.13

require (
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200719141450-e53abe583345
	gitlab.com/accesscontroller/ms-ad-sessions-adapter/types v0.0.0-20200719093937-55bc959410d1
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
