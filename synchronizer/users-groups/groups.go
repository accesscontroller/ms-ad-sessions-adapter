package usersgroups

import (
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/types"
)

// deleteExtGroups deletes ExternalGroupV1s from API.
func deleteExtGroups(api APIGroupsDeleter, deleteCandidates []storage.ExternalGroupV1, metrics measurer) error {
	if err := api.DeleteExternalGroups(deleteCandidates); err != nil {
		return err
	}

	if err := metrics.AddOperation(len(deleteCandidates), group, delete); err != nil {
		return err
	}

	return nil
}

// createExtGroups creates ExternalGroupV1s on API server.
func createExtGroups(api APIGroupsCreator, createCandidates []storage.ExternalGroupV1, metrics measurer) error {
	if _, err := api.CreateExternalGroups(createCandidates, false); err != nil {
		return err
	}

	if err := metrics.AddOperation(len(createCandidates), group, create); err != nil {
		return err
	}

	return nil
}

func syncGroups(source storage.ResourceNameT,
	referenceGroups map[storage.ResourceNameT]*types.GroupWUsers, api AggAPIGroups, metrics measurer) error {
	// Load API Groups.
	apiGroups, err := api.GetExternalGroups(nil, source)
	if err != nil {
		return err
	}

	// Compute changes.

	// Delete candidates.
	var deleteCandidates = make([]storage.ExternalGroupV1, 0)

	for i := range apiGroups {
		if _, ok := referenceGroups[apiGroups[i].Metadata.Name]; !ok {
			deleteCandidates = append(deleteCandidates, apiGroups[i])
		}
	}

	// Delete.
	if len(deleteCandidates) > 0 {
		if err := deleteExtGroups(api, deleteCandidates, metrics); err != nil {
			return err
		}
	}

	// Create and Update.
	// Prepare Groups.
	var apiGroupsM = make(map[storage.ResourceNameT]*storage.ExternalGroupV1, len(apiGroups))

	for i := range apiGroups {
		var ptr = &apiGroups[i]

		apiGroupsM[ptr.Metadata.Name] = ptr
	}

	/* Do not use any ExternalGroupV1 update because the only interesting information
	which can be obtained from LDAP is a Group Name and Users <=> Groups membership.
	The Name is set on creation.
	Another important information contained in ExternalGroupV1 is its AccessGroupV1 Name but
	the AccessGroupV1 Name is set by administrator and must not be changed by the synchronizer.
	*/
	var createCandidates = make([]storage.ExternalGroupV1, 0)

	for _, refGr := range referenceGroups {
		if _, ok := apiGroupsM[refGr.Group.Metadata.Name]; !ok {
			// Create.
			createCandidates = append(createCandidates, storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1, // Initial value.
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: source,
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: refGr.Group.Metadata.Name,
				},
			})
		}
	}

	// Create.
	if len(createCandidates) > 0 {
		if err := createExtGroups(api, createCandidates, metrics); err != nil {
			return err
		}
	}

	return nil
}

// deleteExtUsers deletes ExternalUserV1s from API server.
func deleteExtUsers(api APIUsersDeleter, deleteCandidates []storage.ExternalUserV1, metrics measurer) error {
	if err := api.DeleteExternalUsers(deleteCandidates); err != nil {
		return err
	}

	if err := metrics.AddOperation(len(deleteCandidates), user, delete); err != nil {
		return err
	}

	return nil
}

// createExtUsers creates ExternalUserV1s on API server.
func createExtUsers(api APIUsersCreator, createCandidates []storage.ExternalUserV1, metrics measurer) error {
	if _, err := api.CreateExternalUsers(createCandidates, false); err != nil {
		return err
	}

	if err := metrics.AddOperation(len(createCandidates), user, create); err != nil {
		return err
	}

	return nil
}

func syncUsers(source storage.ResourceNameT,
	referenceGroups map[storage.ResourceNameT]*types.GroupWUsers, api AggAPIUsers, metrics measurer) error {
	// Load API Users.
	apiUsers, err := api.GetExternalUsers(nil, source)
	if err != nil {
		return err
	}

	// Make Users Map for further processing.
	var referenceUsers = make(map[storage.ResourceNameT]bool)

	for _, gr := range referenceGroups {
		for i := range gr.Users {
			referenceUsers[gr.Users[i]] = true
		}
	}

	// Compute delete candidates
	// and delete.
	var deleteCandidates = make([]storage.ExternalUserV1, 0)

	for i := range apiUsers {
		up := &apiUsers[i]

		_, ok := referenceUsers[up.Metadata.Name]
		if !ok { // Delete candidate.
			deleteCandidates = append(deleteCandidates, *up)
		}
	}

	// Delete.
	if len(deleteCandidates) > 0 {
		if err := deleteExtUsers(api, deleteCandidates, metrics); err != nil {
			return err
		}
	}

	// Prepare API Users for processing.
	var apiUsersM = make(map[storage.ResourceNameT]*storage.ExternalUserV1, len(apiUsers))

	for i := range apiUsers {
		var pu = &apiUsers[i]

		apiUsersM[pu.Metadata.Name] = pu
	}

	// Compute Create candidates.
	/* Do not use any ExternalUserV1 update because the only interesting information
	which can be obtained from LDAP is a User Name and Users <=> Groups membership.
	The Name is set on creation and though renaming is possible but the name is the unique ID of the User
	so it must not be changed.
	*/
	var createCandidates = make([]storage.ExternalUserV1, 0)

	for userName := range referenceUsers {
		if _, ok := apiUsersM[userName]; !ok {
			// Create.
			createCandidates = append(createCandidates, storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1, // Start Value
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: source,
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: userName,
				},
				Data: storage.ExternalUserDataV1{},
			})
		}
	}

	// Create.
	if len(createCandidates) > 0 {
		if err := createExtUsers(api, createCandidates, metrics); err != nil {
			return err
		}
	}

	return nil
}

// createExtGroup2UsersRels creates ExternalGroupV1 to ExternalUserV1s membership relations from API.
func createExtGroup2UsersRels(api APIGroup2UsersRelationsCreator,
	relsName, externalGroup, source storage.ResourceNameT,
	externalUsers []storage.ResourceNameT, metrics measurer) error {
	if _, err := api.CreateExternalGroup2ExternalUserListV1Relations(
		&storage.ExternalGroup2ExternalUserListRelationsV1{
			Metadata: storage.MetadataV1{
				APIVersion: storage.APIVersionV1Value,
				ETag:       1,
				Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
				Name:       relsName,
			},
			Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
				ExternalGroup:             externalGroup,
				ExternalUsers:             externalUsers,
				ExternalUsersGroupsSource: source,
			},
		}, false); err != nil {
		return err
	}

	if err := metrics.AddOperation(len(externalUsers), group2user, create); err != nil {
		return err
	}

	return nil
}

// deleteExtGroup2UsersRels deletes ExternalGroupV1 to ExternalUserV1s membership relations from API.
func deleteExtGroup2UsersRels(api APIGroup2UsersRelationsDeleter,
	relsName, externalGroup, source storage.ResourceNameT,
	externalUsers []storage.ResourceNameT, metrics measurer) error {
	if err := api.DeleteExternalGroup2ExternalUserListV1Relations(
		&storage.ExternalGroup2ExternalUserListRelationsV1{
			Metadata: storage.MetadataV1{
				APIVersion: storage.APIVersionV1Value,
				ETag:       1,
				Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
				Name:       relsName,
			},
			Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
				ExternalGroup:             externalGroup,
				ExternalUsers:             externalUsers,
				ExternalUsersGroupsSource: source,
			},
		}); err != nil {
		return err
	}

	if err := metrics.AddOperation(len(externalUsers), group2user, delete); err != nil {
		return err
	}

	return nil
}

func syncGroupUsersRelations(referenceGroups map[storage.ResourceNameT]*types.GroupWUsers,
	api APIGroup2UsersRelationsCRUD, metrics measurer) error {
	// For every Group perform Relations update.
	for _, refGroup := range referenceGroups {
		// Load API Relations.
		apiRelations, err := api.GetExternalGroup2ExternalUserListV1Relations(
			refGroup.Group.Metadata.Name,
			refGroup.Group.Metadata.ExternalSource.SourceName)
		if err != nil {
			return err
		}

		var (
			refUsers = make(map[storage.ResourceNameT]bool, len(refGroup.Users))
			apiUsers = make(map[storage.ResourceNameT]bool, len(apiRelations.Data.ExternalUsers))
		)

		// Prepare sets.
		for i := range refGroup.Users {
			refUsers[refGroup.Users[i]] = true
		}

		for i := range apiRelations.Data.ExternalUsers {
			apiUsers[apiRelations.Data.ExternalUsers[i]] = true
		}

		var (
			createCandidates = make([]storage.ResourceNameT, 0)
			deleteCandidates = make([]storage.ResourceNameT, 0)
		)

		// Create.
		for refUN := range refUsers {
			_, ok := apiUsers[refUN]
			if !ok {
				createCandidates = append(createCandidates, refUN)
			}
		}

		if len(createCandidates) > 0 {
			if err := createExtGroup2UsersRels(api,
				apiRelations.Metadata.Name, refGroup.Group.Metadata.Name,
				refGroup.Group.Metadata.ExternalSource.SourceName, createCandidates,
				metrics); err != nil {
				return err
			}
		}

		// Delete.
		for apiUN := range apiUsers {
			_, ok := refUsers[apiUN]
			if !ok {
				deleteCandidates = append(deleteCandidates, apiUN)
			}
		}

		if len(deleteCandidates) > 0 {
			if err := deleteExtGroup2UsersRels(api,
				apiRelations.Metadata.Name, refGroup.Group.Metadata.Name,
				refGroup.Group.Metadata.ExternalSource.SourceName, deleteCandidates,
				metrics); err != nil {
				return err
			}
		}
	}

	return nil
}
