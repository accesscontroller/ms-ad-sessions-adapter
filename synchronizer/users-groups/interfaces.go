package usersgroups

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/ms-ad-sessions-adapter/types"
)

// APIGroupsLoader defines an interface to load ExternalGroupV1s from API Server.
type APIGroupsLoader interface {
	// GetExternalGroups loads ExternalGroupV1s from API Server.
	// If names are given then only given groups are loaded.
	GetExternalGroups(names []storage.ResourceNameT, source storage.ResourceNameT) ([]storage.ExternalGroupV1, error)
}

// APIGroupsLoader defines an interface to load ExternalGroupV1 from API Server.
// Returns storage.ErrResourceNotFound{} error on not found.
// type APIGroupLoader interface {
// 	// GetExternalGroup loads one ExternalGroupV1 from API Server.
// 	GetExternalGroup(name, source storage.ResourceNameT) (*storage.ExternalGroupV1, error)

// 	// GetExternalGroup2ExternalUserListV1Relations loads all ExternalUserV1s which are members of the given group.
// 	GetExternalGroup2ExternalUserListV1Relations(
// 		name, source storage.ResourceNameT) (*storage.ExternalGroup2ExternalUserListRelationsV1, error)
// }

// APIGroupsCreator defines an interface to create new ExternalGroupV1s.
type APIGroupsCreator interface {
	// CreateExternalGroups creates all given ExternalGroupV1s on API Server.
	CreateExternalGroups(groups []storage.ExternalGroupV1, loadCreated bool) ([]storage.ExternalGroupV1, error)
}

// APIGroupsUpdater defines an interface to update existing ExternalGroupV1s.
type APIGroupsUpdater interface {
	// UpdateExternalGroups updates all given ExternalGroupV1s on API Server.
	UpdateExternalGroups(groups []storage.ExternalGroupV1, loadUpdated bool) ([]storage.ExternalGroupV1, error)
}

// // APIGroupCreator defines an interface to create a new ExternalGroupV1.
// type APIGroupCreator interface {
// 	// CreateExternalGroup creates one ExternalGroupV1 on API Server.
// 	CreateExternalGroup(group *storage.ExternalGroupV1, loadCreated bool) (*storage.ExternalGroupV1, error)
// }

// APIGroupUpdater defines an interface to update an existing ExternalGroupV1.
type APIGroupUpdater interface {
	// UpdateExternalGroup updates an existing ExternalGroupV1.
	// Update could have new Metadata#Name values.
	UpdateExternalGroup(oldName storage.ResourceNameT,
		patch *storage.ExternalGroupV1, loadUpdated bool) (*storage.ExternalGroupV1, error)
}

// APIGroupDeleter defines an interface to delete an existing ExternalGroupV1.
// type APIGroupDeleter interface {
// 	// DeleteExternalGroup deletes an existing ExternalGroupV1 from API Server.
// 	DeleteExternalGroup(*storage.ExternalGroupV1) error
// }

// APIGroupsDeleter defines an interface to delete existing ExternalGroupV1s.
type APIGroupsDeleter interface {
	// DeleteExternalGroups deletes all given ExternalGroupV1s from API Server.
	DeleteExternalGroups(groups []storage.ExternalGroupV1) error
}

// APIUsersLoader defines an interface to load existing ExternalUserV1s.
type APIUsersLoader interface {
	// GetExternalUsers loads all ExternalUserV1s from API source without groups.
	// if filter is empty or nil - loads all Users.
	GetExternalUsers(filter []storage.ExternalUserFilterV1,
		usersGroupsSource storage.ResourceNameT) ([]storage.ExternalUserV1, error)
}

// APIUserLoader defines an interface to load an existing ExternalUserV1.
// type APIUserLoader interface {
// 	// GetExternalUser loads one ExternalUserV1 from API Server.
// 	// Returns storage.ErrResourceNotFound{} on not found.
// 	GetExternalUser(name, source storage.ResourceNameT) (*storage.ExternalUserV1, error)
// }

// APIUserCreator defines an interface to create a new ExternalUserV1.
// type APIUserCreator interface {
// 	// CreateExternalUser creates a new ExternalUserV1 on API Server.
// 	CreateExternalUser(storage.ExternalUserV1) error
// }

// APIUsersCreator defines an interface to create new ExternalUserV1s.
type APIUsersCreator interface {
	// CreateExternalUsers creates multiple ExternalUserV1 on API Server.
	CreateExternalUsers(users []storage.ExternalUserV1, loadCreated bool) ([]storage.ExternalUserV1, error)
}

// APIUserDeleter defines an interface to delete an existing ExternalUserV1.
// type APIUserDeleter interface {
// 	// DeleteExternalUser deletes one existing ExternalUserV1 from API Server.
// 	DeleteExternalUser(u *storage.ExternalUserV1) error
// }

// APIUsersDeleter defines an interface to delete existing ExternalUserV1s.
type APIUsersDeleter interface {
	// DeleteExternalUsers deletes given ExternalUserV1s from API Server.
	DeleteExternalUsers(users []storage.ExternalUserV1) error
}

// // APIUserUpdater defines an interface to update an existing ExternalUserV1.
// type APIUserUpdater interface {
// 	UpdateExternalUser(oldName storage.ResourceNameT, _ storage.ExternalUserV1) error
// }

// APIUsersUpdater defines an interface to update ExternalUserV1s slice.
type APIUsersUpdater interface {
	// UpdateExternalUsers updates give ExternalUserV1s.
	UpdateExternalUsers(users []storage.ExternalUserV1, loadUpdated bool) ([]storage.ExternalUserV1, error)
}

// APIGroup2UsersRelationsLoader defines an interface to load an existing ExternalGroupV1 User-Members.
type APIGroup2UsersRelationsLoader interface {
	GetExternalGroup2ExternalUserListV1Relations(
		name, source storage.ResourceNameT) (*storage.ExternalGroup2ExternalUserListRelationsV1, error)
}

// APIGroup2UsersRelationsCreator defines an interface to create new ExternalGroupV1 to ExternalUserV1 relations.
type APIGroup2UsersRelationsCreator interface {
	// CreateExternalGroup2ExternalUserListV1Relations creates new ExternalGroupV1 to ExternalUserV1s relations.
	CreateExternalGroup2ExternalUserListV1Relations(
		relations *storage.ExternalGroup2ExternalUserListRelationsV1,
		loadCreated bool) (*storage.ExternalGroup2ExternalUserListRelationsV1, error)
}

// APIGroup2UsersRelationsDeleter defines an interface to delete ExternalGroupV1 to ExternalUserV1 relations.
type APIGroup2UsersRelationsDeleter interface {
	// DeleteExternalGroup2ExternalUserListV1Relations deletes existing ExternalGroupV1 to ExternalUserV1s relations.
	DeleteExternalGroup2ExternalUserListV1Relations(
		relations *storage.ExternalGroup2ExternalUserListRelationsV1) error
}

// APIGroup2UsersRelationsCRUD defines interface to CRUD ExternalGroupV1 to ExternalUserV1s relations.
type APIGroup2UsersRelationsCRUD interface {
	APIGroup2UsersRelationsLoader
	APIGroup2UsersRelationsCreator
	APIGroup2UsersRelationsDeleter
}

// type APIUserCRUD interface {
// 	APIUserUpdater
// 	APIUserLoader
// 	APIUserDeleter
// 	APIUserCreator
// }

// type APIUsersCRUD interface {
// 	APIUsersDeleter
// 	APIUsersCreator
// 	APIUsersLoader
// }

type APIGroupsCRUD interface {
	APIGroupsCreator
	APIGroupsUpdater
	APIGroupsLoader
	APIGroupsDeleter

	APIGroup2UsersRelationsCRUD
}

// type APIGroupCRUD interface {
// 	APIGroupCreator
// 	APIGroupUpdater
// 	APIGroupDeleter
// 	APIGroupLoader

// 	APIGroup2UsersRelationsCRUD
// }

type AggAPIGroups interface {
	// APIGroupCRUD
	APIGroupsCRUD
}

type AggAPIUsers interface {
	APIUsersCreator
	APIUsersDeleter
	APIUsersLoader
	APIUsersUpdater

	// APIUserCRUD
}

// RefUsersLoader contains methods to load ExternalUserV1s from a Reference source.
type RefUsersLoader interface {
	// LoadUsers loads all ExternalUserV1s from reference source without groups.
	// if names are not nil and len(names) > 0 then uses the names are ExternalUserV1
	// filter, otherways returns all ExternalUserV1s.
	LoadUsers(names []storage.ResourceNameT, withGroups bool) (map[storage.ResourceNameT]*types.UserWGroups, error)
}

// RefUserLoader contains methods to load one ExternalUserV1 from a Reference source.
// type RefUserLoader interface {
// 	// LoadUser loads one ExternalUserV1, returns storage.ErrResourceNotFound{} on not found.
// 	LoadUser(name storage.ResourceNameT, withGroups bool) (*types.UserWGroups, error)
// }

// AggRefUsersLoader contains methods to load ExternalUserV1s from a Reference source.
type AggRefUsersLoader interface {
	// RefUserLoader
	RefUsersLoader
}

// type RefGroupLoader interface {
// 	// LoadGroup loads an ExternalGroupV1 from reference source.
// 	// Returns nil pointer without error on not found.
// 	LoadGroup(name storage.ResourceNameT, withUsers bool) (*types.GroupWUsers, error)
// }

// RefGroupsLoader contains methods to load ExternalGroupV1s from a Reference source.
type RefGroupsLoader interface {
	// LoadGroups loads ExternalGroupV1s from a Reference;
	// if names are not nil and len(names) > 0 then uses the names are ExternalGroupV1
	// filter, otherways returns all ExternalGroupV1s.
	LoadGroups(names []storage.ResourceNameT, withUsers bool) (map[storage.ResourceNameT]*types.GroupWUsers, error)
}

// AggRefGroupsLoader contains methods to load ExternalGroupV1s from a Reference source.
type AggRefGroupsLoader interface {
	RefGroupsLoader
	// RefGroupLoader
}

// Internal interfaces.

type resultCounter interface {
	// CountResult counts success/error rate. true - success; false - error.
	CountResult(bool)
}

type operationCounter interface {
	// AddOperation counts operations create/update/delete results
	AddOperation(int, metricObjectKind, metricObjectOperation) error
}

type executionTimeMeasurer interface {
	// Duration returns observer which measures process duration.
	Duration() prometheus.Observer
}

type lockWaitMeasuer interface {
	// LockWaitreturns observer which measures process lock waiting duration.
	LockWait() prometheus.Observer
}

// measurer contains methods to collect metrics during process execution.
type measurer interface {
	resultCounter
	operationCounter
	executionTimeMeasurer
	lockWaitMeasuer
}
