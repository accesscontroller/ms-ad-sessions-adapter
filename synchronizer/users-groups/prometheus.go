package usersgroups

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	// prometheusNamespace defines Prometheus metrics namespace.
	prometheusNamespace string = "MSADSessionsAdapter"

	// prometheusSubsystem defines Prometheus metrics subsystem.
	prometheusSubsystem string = "UsersGroupsSynchronizer"

	// prometheusResultCounterName defines Prometheus operation result metric name.
	prometheusResultCounterName string = "Result"

	// prometheusDurationCounterName defines Prometheus operation duration metric name.
	prometheusDurationCounterName string = "Duration"

	// prometheusChangedObjectsCounterName defines Prometheus changed objects metric name.
	prometheusChangedObjectsCounterName string = "Changes"

	// prometheusLockWaitMetricName defines Prometheus Lock Wait metric name.
	prometheusLockWaitMetricName string = "LockWait"

	// prometheusScheduledMetricLV defines a name for metrics to measure scheduled sync operations.
	prometheusScheduledMetricLV string = "Scheduled"

	// prometheusOnDemandMetricLV defines a name for metrics to measure on demand sync operations.
	prometheusOnDemandMetricLV string = "OnDemand"
)

const (
	// syncMetricResultLabel defines name of label which contains operation result status.
	syncMetricResultLabel string = "Result"

	// syncMetricInstanceLabelName defines name of label to indicate instance name.
	syncMetricInstanceLabelName string = "Instance"

	// syncMetricJobTypeLabelName defines name of label to indicate job-type value.
	syncMetricJobTypeLabelName string = "JobType"

	// syncMetricSuccessLabelValue operation success result for label syncMetricResultLabel.
	syncMetricSuccessLabelValue string = "Success"
	// syncMetricFailureLabelValue operation failure result for label syncMetricResultLabel.
	syncMetricFailureLabelValue string = "Failure"
)

// gaugeObserver implements prometheus.Observer interface for prometheus.Timer.
type gaugeObserver struct {
	g prometheus.Gauge
}

// Observe prometheus.Observer implementation.
func (o gaugeObserver) Observe(v float64) {
	o.g.Set(v)
}

const (
	// syncMetricObjectKindLabelName defines a label name which shows object kind
	// for which operation was performed.
	syncMetricObjectKindLabelName string = "kind"

	// syncMetricObjectKindLabelUser an ExternalUserV1 object for syncMetricObjectKindLabelName.
	syncMetricObjectKindLabelUser string = "user"
	// syncMetricObjectKindLabelGroup an ExternalGroupV1 object for syncMetricObjectKindLabelName.
	syncMetricObjectKindLabelGroup string = "group"

	// syncMetricObjectKindLabelGroup2UserRelation an ExternalGroupV1 to an ExternalUserV1
	// relation for syncMetricObjectKindLabelName.
	syncMetricObjectKindLabelGroup2UserRelation string = "group2user"

	// syncMetricObjectOperationLabelName defines a label name for operation
	// type for metrics.
	syncMetricObjectOperationLabelName string = "operation"

	// syncMetricObjectOperationLabelUpdateValue the syncMetricObjectOperationLabelName
	// value for update operations.
	syncMetricObjectOperationLabelUpdateValue string = "update"
	// syncMetricObjectOperationLabelUpdateValue the syncMetricObjectOperationLabelName
	// value for create operations.
	syncMetricObjectOperationLabelCreateValue string = "create"
	// syncMetricObjectOperationLabelUpdateValue the syncMetricObjectOperationLabelName
	// value for delete operations.
	syncMetricObjectOperationLabelDeleteValue string = "delete"
)

// metricObjectKind defines kind of object to count.
type metricObjectKind int

// defines type of object to compute statistics for.
const (
	// user - kind of object to measure ExternalUserV1 operations.
	user metricObjectKind = 1
	// group - kind of object to measure ExternalGroupV1 operations.
	group metricObjectKind = 2

	// group2user - group to user relation.
	group2user metricObjectKind = 3
)

// metricObjectOperation defines operation which was performed with objects.
type metricObjectOperation int

// defines type of operation for metricObjectKind object to compute statistics for.
const (
	// update defines update operation value.
	update metricObjectOperation = 1
	// delete defines delete operation value.
	delete metricObjectOperation = 2
	// create defines create operation value.
	create metricObjectOperation = 3
)

// syncMetric contains set of metrics to meause one internal operation type.
type syncMetric struct {
	// result counts successes and failures.
	result *prometheus.CounterVec

	// objects count of updated, deleted, created objects.
	objects *prometheus.CounterVec

	// duration measures operation time.
	duration gaugeObserver

	// lockWait measures time which an operation had to wait before its start because of
	// locks.
	lockWait gaugeObserver
}

// AddOperation increases counter for given object and operation.
func (sm *syncMetric) AddOperation(v int, kind metricObjectKind, operation metricObjectOperation) error {
	var (
		objectKind      string
		objectOperation string
	)

	// Metric kind label
	switch kind {
	case user:
		objectKind = syncMetricObjectKindLabelUser
	case group:
		objectKind = syncMetricObjectKindLabelGroup
	case group2user:
		objectKind = syncMetricObjectKindLabelGroup2UserRelation
	default:
		panic(fmt.Errorf("incorrect kind value %q", kind))
	}

	// Metric operation label
	switch operation {
	case update:
		objectOperation = syncMetricObjectOperationLabelUpdateValue
	case delete:
		objectOperation = syncMetricObjectOperationLabelDeleteValue
	case create:
		objectOperation = syncMetricObjectOperationLabelCreateValue
	default:
		panic(fmt.Errorf("incorrect operation value %q", operation))
	}

	// Get Metric
	lbl := prometheus.Labels{
		syncMetricObjectKindLabelName:      objectKind,
		syncMetricObjectOperationLabelName: objectOperation,
	}

	metric, err := sm.objects.GetMetricWith(lbl)
	if err != nil {
		return fmt.Errorf("can not get metric with %+v: %w", lbl, err)
	}

	metric.Add(float64(v))

	return nil
}

// Count counts result.
func (sm *syncMetric) CountResult(success bool) {
	var lv string

	if success {
		lv = syncMetricSuccessLabelValue
	} else {
		lv = syncMetricFailureLabelValue
	}

	c, err := sm.result.GetMetricWithLabelValues(lv)
	if err != nil {
		panic(fmt.Errorf("unexpected panic at syncMetric#result#GetMetricWithLabelValues"))
	}

	c.Inc()
}

// Duration returns Observer to measure process duration.
func (sm *syncMetric) Duration() prometheus.Observer {
	return sm.duration
}

// Duration returns Observer to measure process waiting for lock duration.
func (sm *syncMetric) LockWait() prometheus.Observer {
	return sm.lockWait
}

/* newSyncMetric creates a new syncMetric.
instanceName defines prometheus.Labels#instance value.
typeLabelValue defines prometheus metric job-type.
*/
func newSyncMetric(instanceName, typeLabelValue string, reg prometheus.Registerer) (*syncMetric, error) {
	var (
		sm  syncMetric
		err error
	)

	// Result vector.
	sm.result, err = initResultZeroCounterVecMetric(instanceName, typeLabelValue,
		prometheusResultCounterName, reg)
	if err != nil {
		return nil, err
	}

	// Duration.
	sm.duration, err = newZeroGaugeObserver(instanceName, typeLabelValue,
		prometheusDurationCounterName, reg)
	if err != nil {
		return nil, err
	}

	// Lock wait.
	sm.lockWait, err = newZeroGaugeObserver(instanceName, typeLabelValue,
		prometheusLockWaitMetricName, reg)
	if err != nil {
		return nil, err
	}

	// Objects create/update/delete counter.
	sm.objects, err = newObjectsZeroCounterVecMetric(instanceName, typeLabelValue,
		prometheusChangedObjectsCounterName, reg)
	if err != nil {
		return nil, err
	}

	return &sm, nil
}

// newObjectsZeroCounterVecMetric creates a new prometheus.CounterVec with configuration
// to store object synchronization count statistics.
func newObjectsZeroCounterVecMetric(instanceName, typeLabelValue, metricName string,
	reg prometheus.Registerer) (*prometheus.CounterVec, error) {
	const errorPrefix string = "can not init prometheus.CounterVec with instance: %q, name: %q: %w"

	cv := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: prometheusNamespace,
		Subsystem: prometheusSubsystem,
		Name:      metricName,
		Help:      "Counts processing result statistics",
		ConstLabels: prometheus.Labels{
			syncMetricInstanceLabelName: instanceName,
			syncMetricJobTypeLabelName:  typeLabelValue,
		},
	}, []string{syncMetricObjectKindLabelName, syncMetricObjectOperationLabelName})

	var (
		operations = []string{
			syncMetricObjectOperationLabelCreateValue,
			syncMetricObjectOperationLabelUpdateValue,
			syncMetricObjectOperationLabelDeleteValue}

		kinds = []string{syncMetricObjectKindLabelUser, syncMetricObjectKindLabelGroup}
	)

	for _, operation := range operations {
		for _, kind := range kinds {
			c, err := cv.GetMetricWith(prometheus.Labels{
				syncMetricObjectKindLabelName:      kind,
				syncMetricObjectOperationLabelName: operation,
			})
			if err != nil {
				return nil, fmt.Errorf(errorPrefix, instanceName, typeLabelValue, err)
			}

			c.Add(0)
		}
	}

	// Register
	if err := reg.Register(cv); err != nil {
		return nil, fmt.Errorf("can not register metrics in prometheus.Registerer: %w", err)
	}

	return cv, nil
}

// initResultZeroCounterVecMetric creates a new zero counter with success and failure
// labels set to 0.
func initResultZeroCounterVecMetric(instanceName, typeLabelValue, metricName string,
	reg prometheus.Registerer) (*prometheus.CounterVec, error) {
	const errorPrefix string = "can not init prometheus.CounterVec with instance: %q, name: %q: %w"

	cv := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: prometheusNamespace,
		Subsystem: prometheusSubsystem,
		Name:      metricName,
		Help:      "Counts processing result statistics",
		ConstLabels: prometheus.Labels{
			syncMetricInstanceLabelName: instanceName,
			syncMetricJobTypeLabelName:  typeLabelValue,
		},
	}, []string{syncMetricResultLabel})

	// Success label.
	sRes, err := cv.GetMetricWithLabelValues(syncMetricSuccessLabelValue)
	if err != nil {
		return nil, fmt.Errorf(errorPrefix, instanceName, metricName, err)
	}

	sRes.Add(0)

	// Failure label.
	fRes, err := cv.GetMetricWithLabelValues(syncMetricFailureLabelValue)
	if err != nil {
		return nil, fmt.Errorf(errorPrefix, instanceName, metricName, err)
	}

	fRes.Add(0)

	// Register
	if err := reg.Register(cv); err != nil {
		return nil, fmt.Errorf("can not register metrics in prometheus.Registerer: %w", err)
	}

	return cv, nil
}

// newZeroGaugeObserver creates a new gaugeObserver.
// instanceName defines prometheus.Labels#instance value.
// metricName defines prometheus metric Name.
func newZeroGaugeObserver(instanceName, typeLabelValue, metricName string,
	reg prometheus.Registerer) (gaugeObserver, error) {
	g := prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: prometheusNamespace,
		Subsystem: prometheusSubsystem,
		Name:      metricName,
		Help:      "Counts processing result statistics",
		ConstLabels: prometheus.Labels{
			syncMetricInstanceLabelName: instanceName,
			syncMetricJobTypeLabelName:  typeLabelValue,
		},
	})

	g.Set(0)

	// Register
	if err := reg.Register(g); err != nil {
		return gaugeObserver{}, fmt.Errorf("can not register metrics in prometheus.Registerer: %w", err)
	}

	return gaugeObserver{g: g}, nil
}

// metricsSet defines full set of metrics for usersgroups.
type metricsSet struct {
	syncLoop     *syncMetric
	syncOnDemand *syncMetric
}

// newMetricsSet creates a new set of metrics for measuring usersgroups.
func newMetricsSet(instanceName string, reg prometheus.Registerer) (*metricsSet, error) {
	var (
		ms  metricsSet
		err error
	)

	ms.syncLoop, err = newSyncMetric(instanceName, prometheusScheduledMetricLV, reg)
	if err != nil {
		return nil, err
	}

	ms.syncOnDemand, err = newSyncMetric(instanceName, prometheusOnDemandMetricLV, reg)
	if err != nil {
		return nil, err
	}

	return &ms, nil
}
