package usersgroups

import (
	"fmt"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// Syncer implements ExternalUserV1 and ExternalGroupV1
// synchronization with ExternalUsersGroupsSourceV1.
type Syncer struct {
	// API client.
	storageUsers  AggAPIUsers
	storageGroups AggAPIGroups

	// Reference.
	refUsers  AggRefUsersLoader
	refGroups AggRefGroupsLoader

	// Groups Filter.
	groupsFilter []storage.ResourceNameT

	// Metrics.
	metrics *metricsSet

	// Helping data. Can be get from API groups but I think that it is better
	// to set the parameter using New() in order to show the user that they
	// are working with given ExternalUsersGroupsSourceV1.
	externalSource storage.ResourceNameT

	lock sync.Mutex
}

// New creates a new Syncer;
// instanceName must be unique for an application because it defines prometheus
// metrics name.
func New(instanceName string, reg prometheus.Registerer,
	externalSource storage.ResourceNameT,
	uAPI AggAPIUsers, gAPI AggAPIGroups,
	refUsers AggRefUsersLoader, refGroups AggRefGroupsLoader,
	groupsFilter []storage.ResourceNameT) (*Syncer, error) {
	// Create metrics
	ms, err := newMetricsSet(instanceName, reg)
	if err != nil {
		return nil, fmt.Errorf("can not init metrics: %w", err)
	}

	return &Syncer{
		storageUsers:   uAPI,
		storageGroups:  gAPI,
		refUsers:       refUsers,
		refGroups:      refGroups,
		metrics:        ms,
		groupsFilter:   groupsFilter,
		externalSource: externalSource,
	}, nil
}

// StartSyncLoop starts periodic synchronization.
func (s *Syncer) SyncGroups() error {
	// Lock.
	s.lock.Lock()
	defer s.lock.Unlock()

	if err := reloadGroupsMeasuredSynchronized(s.externalSource,
		s.refGroups, s.storageGroups, s.groupsFilter,
		s.storageUsers, s.metrics.syncLoop); err != nil {
		return err
	}

	return nil
}

func reloadGroupsMeasuredSynchronized(
	groupsUsersSource storage.ResourceNameT,
	refGroupsAPI RefGroupsLoader,
	storageGroups AggAPIGroups,
	groupNamesFilter []storage.ResourceNameT,
	storageUsersAPI AggAPIUsers,
	metrics measurer) error {
	// Load Reference Groups and Users.
	referenceGroups, err := refGroupsAPI.LoadGroups(groupNamesFilter, true)
	if err != nil {
		metrics.CountResult(false)

		return err
	}

	// Sync Groups.
	if err := syncGroups(groupsUsersSource, referenceGroups, storageGroups, metrics); err != nil {
		metrics.CountResult(false)

		return err
	}

	// Sync Users.
	if err := syncUsers(groupsUsersSource, referenceGroups, storageUsersAPI, metrics); err != nil {
		metrics.CountResult(false)

		return err
	}

	// Sync Users-Groups relations.
	if err := syncGroupUsersRelations(referenceGroups, storageGroups, metrics); err != nil {
		metrics.CountResult(false)

		return err
	}

	// Measure.
	metrics.CountResult(true)

	return nil
}
