package types

import "gitlab.com/accesscontroller/accesscontroller/controller/storage"

// GroupWUsers contains one ExternalGroupV1 with its User members Names.
type GroupWUsers struct {
	// Group contains one ExternalGroupV1.
	Group *storage.ExternalGroupV1

	// Users contains ExternalUserV1 names.
	Users []storage.ResourceNameT
}

// UserWGroups contains one ExternalUserV1 with Group Names to which the User is added.
type UserWGroups struct {
	// User contains one ExternalUserV1.
	User *storage.ExternalUserV1

	// Groups contains ExternalUserV1 memberof groups.
	Groups []storage.ResourceNameT
}
