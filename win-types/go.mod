module gitlab.com/accesscontroller/ms-ad-sessions-adapter/win-types

go 1.13

require (
	github.com/0xrawsec/golang-utils v1.1.8 // indirect
	github.com/0xrawsec/golang-win32 v1.0.3
)
