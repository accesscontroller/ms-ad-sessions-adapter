// +build windows

package wintypes

import (
	"fmt"
	"strconv"
	"time"

	"github.com/0xrawsec/golang-win32/win32/wevtapi"
)

// RawEvent contains full not parsed Event data.
type RawEvent struct {
	wevtapi.XMLEvent
}

// ParseWinEvent parses RawEvent.
func ParseWinEvent(r *RawEvent) (*Event, error) {
	const errPrefix string = "can not ParseWinEvent: %w"

	var evt Event

	// Parse EventData part.
	ed, err := parseEventData(r.EventData.Data)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.EventData = *ed

	// Parse System part.
	evt.System.Provider.Name = r.System.Provider.Name
	evt.System.Provider.Guid = r.System.Provider.Guid

	evt.System.EventID, err = strconv.ParseInt(r.System.EventID, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Version, err = strconv.ParseInt(r.System.Version, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Level, err = strconv.ParseInt(r.System.Level, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Task, err = strconv.ParseInt(r.System.Task, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Opcode, err = strconv.ParseInt(r.System.Opcode, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Keywords = r.System.Keywords

	evt.System.TimeCreated.SystemTime, err = time.Parse(time.RFC3339Nano, r.System.TimeCreated.SystemTime)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.EventRecordID, err = strconv.ParseInt(r.System.EventRecordID, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Execution.ProcessID, err = strconv.ParseInt(r.System.Execution.ProcessID, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Execution.ThreadID, err = strconv.ParseInt(r.System.Execution.ThreadID, 10, 64)
	if err != nil {
		return nil, fmt.Errorf(errPrefix, err)
	}

	evt.System.Channel = r.System.Channel
	evt.System.Computer = r.System.Computer
	evt.System.Security.UserID = r.System.Security.UserID

	return &evt, nil
}
