// +build windows

package wintypes

import (
	"fmt"
	"regexp"
)

type matchResult int8

const (
	matched        matchResult = 0
	notMatched     matchResult = 1
	regexUndefined matchResult = 2
)

// EventFilterEvaluator method to filter Event.
type EventFilterEvaluator func(e *RawEvent) bool

// FilterRecordSystemPart defines Event#System filter part.
type FilterRecordSystemPart struct {
	Provider struct {
		Name string `xml:"Name,attr"`
		GUID string `xml:"Guid,attr"`
	} `xml:"Provider"`
	EventID  string `xml:"EventID"`
	Version  string `xml:"Version"`
	Level    string `xml:"Level"`
	Task     string `xml:"Task"`
	Opcode   string `xml:"Opcode"`
	Keywords string `xml:"Keywords"`
	// TimeCreated struct {
	// 	SystemTime string `xml:"SystemTime,attr"`
	// } `xml:"TimeCreated"`
	EventRecordID string `xml:"EventRecordID"`
	Execution     struct {
		ProcessID string `xml:"ProcessID,attr"`
		ThreadID  string `xml:"ThreadID,attr"`
	} `xml:"Execution"`
	Channel  string `xml:"Channel"`
	Computer string `xml:"Computer"`
	Security struct {
		UserID string `xml:"UserID,attr"`
	} `xml:"Security"`
}

// FilterRecord contains fields which can be used as Filter patterns.
type FilterRecord struct {
	// System filters for System part of Event.
	System FilterRecordSystemPart

	// ToDo: find an example of UserData to build filtering regex set.
	// UserData defines filter for User part of Event where key is a Field Name and value is a Filter Regex.
	// UserData map[string]string

	// EventData defines filter for EventData part of Event where key is a Field Name and value is a Filter Regex.
	EventData map[string]string
}

// filterSet contains Regexes set to filter Event fields.
type filterSet struct {
	ProviderName *regexp.Regexp
	ProviderGUID *regexp.Regexp

	EventID  *regexp.Regexp
	Version  *regexp.Regexp
	Level    *regexp.Regexp
	Task     *regexp.Regexp
	Opcode   *regexp.Regexp
	Keywords *regexp.Regexp
	// TimeCreatedSystemTime string `xml:"SystemTime,attr"`
	EventRecordID      *regexp.Regexp
	ExecutionProcessID *regexp.Regexp
	ExecutionThreadID  *regexp.Regexp

	Channel        *regexp.Regexp
	Computer       *regexp.Regexp
	SecurityUserID *regexp.Regexp

	// UserData map[string]*regexp.Regexp

	EventData map[string]*regexp.Regexp
}

// Filter contains patterns to filter Event.
type Filter struct {
	ExcludePatterns FilterRecord
	IncludePatterns FilterRecord
}

// ComputeEvaluator creates Filter evaluator.
func ComputeEvaluator(f *Filter) (EventFilterEvaluator, error) {
	includeRegexes, err := makeRegexes(&f.IncludePatterns)
	if err != nil {
		return nil, fmt.Errorf("can not compile Filter#IncludePatterns: %w", err)
	}

	excludeRegexes, err := makeRegexes(&f.ExcludePatterns)
	if err != nil {
		return nil, fmt.Errorf("can not compile Filter#ExcludePatterns: %w", err)
	}

	return makeEvaluator(&includeRegexes, &excludeRegexes), nil
}

func makeEvaluator(include, exclude *filterSet) EventFilterEvaluator {
	return func(e *RawEvent) bool {
		// Check include filters
		if !evaluateFilter(include, e, notMatched) {
			return false
		}

		// Check exclude filters
		if !evaluateFilter(exclude, e, matched) {
			return false
		}

		return true
	}
}

// evaluateFilter checks that all matches for filter set records are satisfied;
// matchToFalse - defines func matchRE() result which causes returning false.
func evaluateFilter(flt *filterSet, evt *RawEvent, matchToFalse matchResult) bool {
	if matchRE(flt.ProviderName, evt.System.Provider.Name) == matchToFalse {
		return false
	}

	if matchRE(flt.ProviderGUID, evt.System.Provider.Guid) == matchToFalse {
		return false
	}

	if matchRE(flt.EventID, evt.System.EventID) == matchToFalse {
		return false
	}

	if matchRE(flt.Version, evt.System.Version) == matchToFalse {
		return false
	}

	if matchRE(flt.Level, evt.System.Level) == matchToFalse {
		return false
	}

	if matchRE(flt.Task, evt.System.Task) == matchToFalse {
		return false
	}

	if matchRE(flt.Opcode, evt.System.Opcode) == matchToFalse {
		return false
	}

	if matchRE(flt.Keywords, evt.System.Keywords) == matchToFalse {
		return false
	}

	if matchRE(flt.EventRecordID, evt.System.EventRecordID) == matchToFalse {
		return false
	}

	if matchRE(flt.ExecutionProcessID, evt.System.Execution.ProcessID) == matchToFalse {
		return false
	}

	if matchRE(flt.ExecutionThreadID, evt.System.Execution.ThreadID) == matchToFalse {
		return false
	}

	if matchRE(flt.Channel, evt.System.Channel) == matchToFalse {
		return false
	}

	if matchRE(flt.Computer, evt.System.Computer) == matchToFalse {
		return false
	}

	if matchRE(flt.SecurityUserID, evt.System.Security.UserID) == matchToFalse {
		return false
	}

	// Match EventData.
	if flt.EventData == nil {
		return true
	}

	for _, n := range evt.EventData.Data {
		// Get Regex.
		rgx, ok := flt.EventData[n.Name]
		if !ok {
			continue
		}

		// Match.
		if matchRE(rgx, n.Value) == matchToFalse {
			return false
		}
	}

	return true
}

// matchRE checks string for given regex.
func matchRE(r *regexp.Regexp, s string) matchResult {
	if r == nil {
		return regexUndefined
	}

	if r.MatchString(s) {
		return matched
	}

	return notMatched
}

// makeRegexes builds filterSet from given FilterRecords.
func makeRegexes(f *FilterRecord) (filterSet, error) {
	// System part.
	filter, err := makeSystemRegexes(f)
	if err != nil {
		return filterSet{}, err
	}

	// UserData part.
	// if f.UserData != nil {

	// }

	// EventData part.
	if f.EventData != nil {
		m, err := makeEventDataRegexes(f.EventData)
		if err != nil {
			return filterSet{}, err
		}

		filter.EventData = m
	}

	return filter, nil
}

// makeSystemRegexes builds filterSet from given FilterRecord for System part of Event.
// Nil valued fields of FilterRecord produce nil *regex.Regexp.
func makeSystemRegexes(f *FilterRecord) (filterSet, error) {
	providerName, err := makeRegex(f.System.Provider.Name)
	if err != nil {
		return filterSet{}, err
	}

	providerGUID, err := makeRegex(f.System.Provider.GUID)
	if err != nil {
		return filterSet{}, err
	}

	eventID, err := makeRegex(f.System.EventID)
	if err != nil {
		return filterSet{}, err
	}

	version, err := makeRegex(f.System.Version)
	if err != nil {
		return filterSet{}, err
	}

	level, err := makeRegex(f.System.Level)
	if err != nil {
		return filterSet{}, err
	}

	task, err := makeRegex(f.System.Task)
	if err != nil {
		return filterSet{}, err
	}

	opcode, err := makeRegex(f.System.Opcode)
	if err != nil {
		return filterSet{}, err
	}

	keywords, err := makeRegex(f.System.Keywords)
	if err != nil {
		return filterSet{}, err
	}

	eventRecordID, err := makeRegex(f.System.EventRecordID)
	if err != nil {
		return filterSet{}, err
	}

	execProcessID, err := makeRegex(f.System.Execution.ProcessID)
	if err != nil {
		return filterSet{}, err
	}

	execThreadID, err := makeRegex(f.System.Execution.ThreadID)
	if err != nil {
		return filterSet{}, err
	}

	channel, err := makeRegex(f.System.Channel)
	if err != nil {
		return filterSet{}, err
	}

	computer, err := makeRegex(f.System.Computer)
	if err != nil {
		return filterSet{}, err
	}

	secUserID, err := makeRegex(f.System.Security.UserID)
	if err != nil {
		return filterSet{}, err
	}

	return filterSet{
		ProviderName:       providerName,
		ProviderGUID:       providerGUID,
		EventID:            eventID,
		Version:            version,
		Level:              level,
		Task:               task,
		Opcode:             opcode,
		Keywords:           keywords,
		EventRecordID:      eventRecordID,
		ExecutionProcessID: execProcessID,
		ExecutionThreadID:  execThreadID,
		Channel:            channel,
		Computer:           computer,
		SecurityUserID:     secUserID,
	}, nil
}

// makeEventDataRegexes build regexes for EventData Event field.
func makeEventDataRegexes(flt map[string]string) (map[string]*regexp.Regexp, error) {
	res := make(map[string]*regexp.Regexp, len(flt))

	for n, r := range flt {
		// Omit empty regexes.
		if r == "" {
			continue
		}

		v, err := regexp.Compile(r)
		if err != nil {
			return nil, fmt.Errorf("can not build Regex for EventData %q:%q: %w",
				n, r, err)
		}

		res[n] = v
	}

	return res, nil
}

// makeRegex builds one Regex for given pattern; if pattern is Zero then returns nil.
func makeRegex(pattern string) (*regexp.Regexp, error) {
	if pattern == "" {
		return nil, nil
	}

	rg, err := regexp.Compile(pattern)
	if err != nil {
		return nil, fmt.Errorf("can not build Regex %q: %w", pattern, err)
	}

	return rg, nil
}
