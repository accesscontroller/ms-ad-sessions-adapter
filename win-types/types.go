// +build windows

/*
Common types for ms-ad-sessions-adapter.

The types are here to split dependencies.
The package also implements simple *regex.Regex Event filter method.
*/
package wintypes

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/0xrawsec/golang-win32/win32/wevtapi"
)

var (
	ErrUnexpectedKind = errors.New("unexpected reflect.Kind in FilterRecord. Fatal. Call developer")
)

const (
	LoggedOnEvent  int64 = 4624
	LoggedOffEvent int64 = 4634
)

// LogonType stores type of Windows Authentication type.
type LogonType int

const (
	// Interactive  (logon at keyboard and screen of system).
	Interactive LogonType = 2
	// Network  (i.e., connection to shared folder on this computer from elsewhere on network).
	Network LogonType = 3
	// Batch  (i.e., scheduled task).
	Batch LogonType = 4
	// Service  (service startup).
	Service LogonType = 5
	// Unlock  (i.e., unattended workstation with password-protected screen saver).
	Unlock LogonType = 7
	// NetworkCleartext  (i.e., logon with credentials sent in clear text;
	// most often indicates a logon to IIS with basic authentication).
	NetworkCleartext LogonType = 8
	// NewCredentials  (e.g., RunAs, mapping a network drive with alternate credentials;
	// does not seem to show up in any events) If you want to track users attempting to logon
	// with alternate credentials see event ID 4648.
	NewCredentials LogonType = 9
	// RemoteInteractive  (e.g., Terminal Services, Remote Desktop, Remote Assistance).
	RemoteInteractive LogonType = 10
	// CachedInteractive  (logon with cached domain credentials such as
	// when logging on to a laptop when away from the network).
	CachedInteractive LogonType = 11
)

// EventData contains parsed EventData.
type EventData struct {
	AuthenticationPackageName string    `xml:"AuthenticationPackageName"`
	ImpersonationLevel        string    `xml:"ImpersonationLevel"`
	IpAddress                 string    `xml:"IpAddress"` // nolint:stylecheck,golint
	IpPort                    uint16    `xml:"IpPort"`    // nolint:stylecheck,golint
	KeyLength                 int       `xml:"KeyLength"`
	LmPackageName             string    `xml:"LmPackageName"`
	LogonGuid                 string    `xml:"LogonGuid"` // nolint:stylecheck,golint
	LogonProcessName          string    `xml:"LogonProcessName"`
	LogonType                 LogonType `xml:"LogonType"`
	ProcessId                 string    `xml:"ProcessId"` // nolint:stylecheck,golint
	ProcessName               string    `xml:"ProcessName"`
	SubjectDomainName         string    `xml:"SubjectDomainName"`
	SubjectLogonId            string    `xml:"SubjectLogonId"` // nolint:stylecheck,golint
	SubjectUserName           string    `xml:"SubjectUserName"`
	SubjectUserSid            string    `xml:"SubjectUserSid"`
	TargetDomainName          string    `xml:"TargetDomainName"`
	TargetLogonId             string    `xml:"TargetLogonId"` // nolint:stylecheck,golint
	TargetUserName            string    `xml:"TargetUserName"`
	TargetUserSid             string    `xml:"TargetUserSid"`
	TransmittedServices       string    `xml:"TransmittedServices"`
	WorkstationName           string    `xml:"WorkstationName"`
}

// parseEventData parses data to populate EventData;
// supports a small subset of data.
func parseEventData(data []wevtapi.Data) (*EventData, error) {
	const errPrefix string = "can not ParseData %s: %w"

	var ed EventData

	for _, v := range data {
		switch v.Name {
		case "AuthenticationPackageName":
			ed.AuthenticationPackageName = v.Value
		case "ImpersonationLevel":
			ed.ImpersonationLevel = v.Value
		case "IpAddress":
			ed.IpAddress = v.Value
		case "IpPort":
			v, err := strconv.ParseUint(v.Value, 10, 16)
			if err != nil {
				return nil, fmt.Errorf(errPrefix, "IPPort", err)
			}

			ed.IpPort = uint16(v)
		case "KeyLength":
			v, err := strconv.ParseInt(v.Value, 10, 32)
			if err != nil {
				return nil, fmt.Errorf(errPrefix, "KeyLength", err)
			}

			ed.KeyLength = int(v)
		case "LmPackageName":
			ed.LmPackageName = v.Value
		case "LogonGuid":
			ed.LogonGuid = v.Value
		case "LogonProcessName":
			ed.LogonProcessName = v.Value
		case "LogonType":
			v, err := strconv.ParseUint(v.Value, 10, 16)
			if err != nil {
				return nil, fmt.Errorf(errPrefix, "LogonType", err)
			}

			ed.LogonType = LogonType(v)
		case "ProcessId":
			ed.ProcessId = v.Value
		case "ProcessName":
			ed.ProcessName = v.Value
		case "SubjectDomainName":
			ed.SubjectDomainName = v.Value
		case "SubjectLogonId":
			ed.SubjectLogonId = v.Value
		case "SubjectUserName":
			ed.SubjectUserName = v.Value
		case "SubjectUserSid":
			ed.SubjectUserSid = v.Value
		case "TargetDomainName":
			ed.TargetDomainName = v.Value
		case "TargetLogonId":
			ed.TargetLogonId = v.Value
		case "TargetUserName":
			ed.TargetUserName = v.Value
		case "TargetUserSid":
			ed.TargetUserSid = v.Value
		case "TransmittedServices":
			ed.TransmittedServices = v.Value
		case "WorkstationName":
			ed.WorkstationName = v.Value
		}
	}

	return &ed, nil
}

// System contains Event System part.
type System struct {
	Provider struct {
		Name string `xml:"Name,attr"`
		Guid string `xml:"Guid,attr"` // nolint:stylecheck,golint
	} `xml:"Provider"`
	EventID     int64  `xml:"EventID"`
	Version     int64  `xml:"Version"`
	Level       int64  `xml:"Level"`
	Task        int64  `xml:"Task"`
	Opcode      int64  `xml:"Opcode"`
	Keywords    string `xml:"Keywords"`
	TimeCreated struct {
		SystemTime time.Time `xml:"SystemTime,attr"`
	} `xml:"TimeCreated"`
	EventRecordID int64 `xml:"EventRecordID"`
	Correlation   struct {
	} `xml:"Correlation"`
	Execution struct {
		ProcessID int64 `xml:"ProcessID,attr"`
		ThreadID  int64 `xml:"ThreadID,attr"`
	} `xml:"Execution"`
	Channel  string `xml:"Channel"`
	Computer string `xml:"Computer"`
	Security struct {
		UserID string `xml:"UserID,attr"`
	} `xml:"Security"`
}

// Event contains parsed Event.
type Event struct {
	// EventData contains parsed EventData.
	// For now contains only fields required for storing Events 4624, 4634.
	EventData EventData `xml:"EventData,omitempty"`
	System    System    `xml:"System"`
}

func (evt *Event) String() string {
	type tType Event

	return fmt.Sprintf("%+v", tType(*evt))
}
